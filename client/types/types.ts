export type ContainerType = {
    id: number
    serial: number
    name: string
    model: string
    status: string
}
export type UserRole = 'lead' | 'client'
export type APIUsersRoles = {
    lead: boolean
    client: boolean
}
export type UsersType = {
    id?: number,
    name: string,
    email: string,
    status: UserRole,
    date: string,
    note: string,
    interests?: any[],
    containers: any[],
}
export type IconPropsType = {
    iconClass: string
}
export enum UserRoles {
    LEAD = 'lead',
    CLIENT = 'client',
}
export enum StatusesColors {
    RED = 'red',
    YELLOW = 'yellow',
    GRAY = 'gray',
    GREEN = 'green'
}

export enum UserRolesColors {
    client = 'blue',
    lead = 'green',
}

export enum ButtonColorThemes {
    blue = 'blue',
    white = 'white',
    red = 'red',
    lightBlue = 'lightBlue'
}

export enum InputSize {
    SM = 'small',
    MD = 'medium',
    LG = 'big',
}

export enum ButtonVariant {
    UNDERLINED = 'underlined',
}
