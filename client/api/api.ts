import {UsersType} from "../types/types";
import {UserFormType, UserType} from "../admin/model";

const baseUrl = '/api'

export const getUsers =  async (): Promise<UserType[]> => {
    const res = await fetch(`${baseUrl}/users`);
    return res.json();
}

export const addUser =  async (user: UserFormType): Promise<UserType> => {
    const res = await fetch(`${baseUrl}/addUser`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
    return res.json();
}

export const updateUser =  async (user: UserType): Promise<UserType> => {
    const res = await fetch(`${baseUrl}/updateUser/${user.id}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
    return res.json();
}

export const deleteUser =  async (id: number): Promise<UserType> => {
    const res = await fetch(`${baseUrl}/deleteUser/${id}`, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    });
    return res.json();
}