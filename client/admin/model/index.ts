import {createEffect, createEvent, createStore, restore} from "effector";
import {createForm} from "effector-forms";
import {addUser, getUsers, updateUser, deleteUser} from "../../api/api";

export const getUsersFx = createEffect<void, UserType[], Error>(() => getUsers())
export const addUserFx = createEffect<UserFormType, UserType, Error>((params) => addUser(params))
export const updateUserFx = createEffect<UserType, UserType, Error>((params) => updateUser(params))
export const deleteUserFx = createEffect<number, UserType, Error>((params) => deleteUser(params))

export const userForm = createForm({
    fields: {
        name: {
            init: '',
        },
        email: {
            init: '',
        },
        status: {
            init: 'lead',
        },
        note: {
            init: ''
        },
        containers: {
            init: []
        }
    },
    validateOn: ["submit"],
})// нужно резетать форму при успешном добавлении пользователя

export const $users = restore<UserType[]>(getUsersFx.doneData,[])
    .on(addUserFx.doneData, (state, data) =>
        [...state, data]
    )
    .on(updateUserFx.done, (state, {params}) => {
        const index = state.findIndex(user => user.id === params.id)
        return [
            ...state.slice(0, index),
            params,
            ...state.slice(index + 1),
        ]
    })
    .on(deleteUserFx.done, (state, {params}) => state.filter(user => user.id !== params))

export const $error = createStore<Error|null>(null)
    .on([addUserFx.failData, updateUserFx.failData, deleteUserFx.failData], (state, error) => error)
    .reset([addUserFx.done, updateUserFx.done, deleteUserFx.done])//нужно делать резет стора при выполнении того эффекта на котором произошла ошибка



export type UserFormType = {
    name: string,
    email: string,
    status: string,
    note: string,
    containers: string[],
}

export type UserType = UserFormType & {
    id: number
}

