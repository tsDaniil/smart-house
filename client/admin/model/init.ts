import {forward} from "effector";
import {addUserFx, userForm} from "./index";


forward({
    from: userForm.formValidated,
    to: addUserFx,
})