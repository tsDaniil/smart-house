import {forward} from "effector";
import { $userEmail, $userId, $userName, checkUserFx, loginForm, loginFx, logoutFx } from './index';

forward({
    from: loginForm.formValidated,
    to: loginFx,
});

$userEmail
    .on([checkUserFx.doneData, loginFx.doneData], (_, data) => data.email)
    .reset(logoutFx.doneData);
$userId
    .on([checkUserFx.doneData, loginFx.doneData], (_, data) => data.id)
    .reset(logoutFx.doneData);
$userName
    .on([checkUserFx.doneData, loginFx.doneData], (_, data) => data.name)
    .reset(logoutFx.doneData);