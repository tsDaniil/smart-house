import type { BackendUser } from '../../../backend/src/types/types';
import { createEffect, createStore } from 'effector';
import { createForm } from 'effector-forms';

export const $userId = createStore<number | null>(null);
export const $userEmail = createStore<string | null>(null);
export const $userName = createStore<string | null>(null);

export const loginFx = createEffect((params: LoginForm) =>
    fetch('/api/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params),
        credentials: 'include'
    })
        .then<Omit<BackendUser, 'password'>>((r) => r.json())
);

export const checkUserFx = createEffect(() =>
    fetch('/api/user', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include'
    })
        .then<Omit<BackendUser, 'password'>>((r) => r.json())
);

export const logoutFx = createEffect(() =>
    fetch('/api/logout', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include'
    })
        .then<{ ok: boolean }>((r) => r.json())
)

export const loginForm = createForm({
    fields: {
        email: {
            init: 'tsddaniil@gmail.com', // field's store initial value
            rules: [
                {
                    name: 'email',
                    validator: (value: string) => /\S+@\S+\.\S+/.test(value)
                },
            ],
        },
        password: {
            init: 'trololo', // field's store initial value
            rules: [
                {
                    name: 'required',
                    validator: (value: string) => Boolean(value),
                }
            ],
        },
    },
    validateOn: ['submit'],
});

type LoginForm = {
    email: string;
    password: string;
}

