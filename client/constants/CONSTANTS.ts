import {DashBoardIcon} from "../components/SideBar/MenuIcons/DashBoardIcon";
import {ContainersIcon} from "../components/InfoBlocks/Icons/ContainersIcon";

export const NAV_ITEMS = [
    {text: 'Dashboard', href: '/', Icon: DashBoardIcon, hasUpdate: true},
    {text: 'Containers', href: '/containers', Icon: ContainersIcon, hasUpdate: false},
]

export const CONTAINERS_TABLE_HEADERS = {
    serial: 'Serial',
    name: 'Name',
    model: 'Model',
    status: 'Status',
}

export const ADMIN_CONTAINERS_TABLE_HEADERS = {
    serial: 'Serial',
    name: 'Name',
    model: 'Model',
    actions: '',
}

export const DOT_COLORS = {
    'Warning': 'yellow',
    'Offline': 'gray',
    'Healthy': 'green',
    'Danger': 'red',
}
export const USERS_TABLE_HEADERS = {
    name: 'Name',
    email: 'E-mail',
    status: 'Status',
    date: 'Date',
    actions: ''
}