import React, {FC, HTMLInputTypeAttribute, useEffect, useState} from "react";
import style from './PasswordTextFiled.module.scss'
import textField from '../TextField/TextField.module.scss'
import {HidePassIcon} from "../AuthorizationForm/Icons/HidePassIcon";
import {ShowPassIcon} from "../AuthorizationForm/Icons/ShowPassIcon";
import {TextFieldPropsType} from "../TextField/TextField";
import {TextFieldWithIcon} from "../TextFieldWithIcon/TextFieldWithIcon";
import {PasswordIcon} from "../AuthorizationForm/Icons/PasswordIcon";

type PasswordTextFieldPropsType = TextFieldPropsType & {
    type: 'password'
};

export const PasswordTextField: FC<PasswordTextFieldPropsType> = (props) => {

    const [inputType, setInputType] = useState<HTMLInputTypeAttribute>('password');

    useEffect(() => {
        console.log(inputType)
    },[inputType])

    return (
        <TextFieldWithIcon Icon={PasswordIcon} {...props} type={inputType} st={[style.pass]}>
            {
                inputType === 'password'
                    ? <div className={style.showPassBtn} onClick={() => setInputType('text')}>
                        <ShowPassIcon iconClass={style.showPassIcon}/>
                    </div>
                    : <div className={style.hidePassBtn} onClick={() => setInputType('password')}>
                        <HidePassIcon iconClass={style.hidePassIcon}/>
                    </div>
            }
        </TextFieldWithIcon>

    );
};

