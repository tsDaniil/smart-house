import React, {FC, useContext} from "react";
import {GlobalModalContext} from "./GlobalModal";
import modal from './Modal.module.scss'
import mainStyle from '../../styles/Main.module.scss'
import {ModalHeader} from "./ModalHeader";
import {Button} from "../Button/Button";
import {TextField} from "../TextField/TextField";
import {Label} from "../TextField/Label";
import {CustomSelect, OptionType} from "../CustomSelect/CustomSelect";
import {ActionMeta} from "react-select/dist/declarations/src/types";
import {InputSize} from "../../types/types";


export const AddContainerModal: FC<{  }> = props => {
    const { hideModal, store } = useContext(GlobalModalContext)
    const { modalProps } = store || {}
    const { modalTitle, id} = modalProps || {}
    const closeHandler = () => {
        hideModal()
    }
    const models = [
        {value: 'ASI-C40HQ', label: 'ASI-C40HQ'},
        {value: 'ASI-T20M', label: 'ASI-T20M'},
        {value: 'ASI-T30M', label: 'ASI-T30M'},
        {value: 'ASI-C20HQ', label: 'ASI-C20HQ'},
    ]
    return (
        <div className={modal.modal}>
            <ModalHeader closeHandler={closeHandler}>{modalTitle}</ModalHeader>
            <div className={modal.modalContent}>
                <div className={mainStyle.mb24}>
                    <Label htmlFor={'addContainerSerial'}>Serial</Label>
                    <TextField type='text' placeholder={'Serial'} inputSize={InputSize.LG} id={'addContainerSerial'}/>
                </div>

                <div className={mainStyle.mb24}>
                    <Label htmlFor={'addContainerName'}>Name</Label>
                    <TextField type='text' placeholder={'Container name'} inputSize={InputSize.LG} id={'addContainerName'}/>
                </div>

                <div className={mainStyle.mb24}>
                    <CustomSelect
                        onChange={(value: readonly OptionType[] | OptionType | null, action: ActionMeta<OptionType>) =>{
                            console.log(value, action)
                        }}
                        defaultValue={models[0]}
                        options={models}
                        name='addContainerName'
                        menuPlacement={'top'}
                    />
                </div>

                <Button fontSize={16}>Add container</Button>
            </div>
        </div>
    );
};