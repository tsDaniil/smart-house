import React, {FC, useContext} from "react";
import {GlobalModalContext} from "./GlobalModal";
import styles from './Modal.module.scss'
import mainStyles from '../../styles/Main.module.scss'
import {ModalHeader} from "./ModalHeader";
import {Button} from "../Button/Button";
import {ButtonColorThemes} from "../../types/types";


export const DeleteModal: FC<{  }> = props => {
    const { hideModal, store } = useContext(GlobalModalContext)
    const { modalProps } = store || {}
    const { modalTitle} = modalProps || {}
    const closeHandler = () => {
        hideModal()
    }
    return (
        <div className={styles.modal}>
            <ModalHeader closeHandler={closeHandler}>{modalTitle}</ModalHeader>
            <div className={styles.modalContent}>
                <div className={mainStyles.mb24}>All data will be lost</div>
                <div className={styles.deleteBtn}>
                    <Button colorTheme={ButtonColorThemes.red} fontSize={16}>Delete user</Button>
                </div>
                <Button
                    colorTheme={ButtonColorThemes.lightBlue}
                    fontSize={16}
                    onClick={() => hideModal()}
                >
                    Cancel
                </Button>
            </div>
        </div>
    );
};