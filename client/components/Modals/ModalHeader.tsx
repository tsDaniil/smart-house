import {FC, ReactNode} from "react";
import mainStyle from '../../styles/Main.module.scss'
import {CloseIcon} from "./Icons/CloseIcon";
import style from './Modal.module.scss'
import cn from "classnames";

type ModalHeaderPropsType = {
    closeHandler: () => void
    children: ReactNode
}

export const ModalHeader: FC<ModalHeaderPropsType> = ({closeHandler, children}) => {
    return (
        <div className={cn(mainStyle.flexRowCenterBetween, mainStyle.mb24)}>
            <h3>{children}</h3>
            <CloseIcon onClick={closeHandler} iconClass={style.closeIcon}/>
        </div>
    );
};