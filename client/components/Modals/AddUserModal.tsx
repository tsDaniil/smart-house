import React, {FC, FormEvent, useContext, useState} from "react";
import {GlobalModalContext} from "./GlobalModal";
import modal from './Modal.module.scss'
import mainStyle from '../../styles/Main.module.scss'
import {ModalHeader} from "./ModalHeader";
import {Button} from "../Button/Button";
import {TextField} from "../TextField/TextField";
import {Label} from "../TextField/Label";
import {RadioBtn} from "../RadioButton/RadioBtn";
import {TextArea} from "../TextArea/TextArea";
import {InputSize, UserRole, UserRoles} from "../../types/types";
import {CustomSelect, OptionType} from "../CustomSelect/CustomSelect";
import {containersMock} from "../../pages";
import {useForm} from "effector-forms";
import {userForm} from "../../admin/model";
import {CustomSelectWithCheckboxes} from "../CustomSelect/CustomSelectWithCheckboxes";
import {ActionMeta} from "react-select/dist/declarations/src/types";


export const AddUserModal: FC<{  }> = props => {
    const [role, setRole] = useState<UserRole | null>(UserRoles.LEAD);
    const { fields, submit } = useForm(userForm)
    const { hideModal, store } = useContext(GlobalModalContext)

    const containers = containersMock.map(container => ({value: container.id, label: container.serial}))
    const { modalProps } = store || {}
    const { modalTitle, id} = modalProps || {}
    const closeHandler = () => {
        hideModal()
    }
    const onSubmit = (e:FormEvent) => {
        e.preventDefault()
        submit()
    }

    return (
        <div className={modal.modal}>
            <ModalHeader closeHandler={closeHandler}>{modalTitle}</ModalHeader>
            <form onSubmit={onSubmit}>
            <div className={modal.modalContent}>
                <div className={mainStyle.mb24}>
                    <Label htmlFor={'addUserName'}>Name</Label>
                    <TextField
                        inputSize={InputSize.LG}
                        type='text'
                        placeholder={'User name'}
                        id={'addUserName'}
                        value={fields.name.value}
                        onChange={(e) => fields.name.onChange(e.target.value)}
                    />
                </div>

                <div className={mainStyle.mb24}>
                    <Label htmlFor={'addUserEmail'}>e-mail</Label>
                    <TextField
                        inputSize={InputSize.LG}
                        type='email'
                        placeholder={'User e-mail'}
                        id={'addUserEmail'}
                        value={fields.email.value}
                        onChange={(e) => fields.email.onChange(e.target.value)}
                    />
                </div>


                <Label>Satus</Label>
                <div className={mainStyle.mb28}>
                    <RadioBtn
                        text='Client'
                        onChange={(e) => {
                            const role = e.target.value
                            setRole(role as UserRole)
                            fields.status.onChange(e.target.value)
                            fields.containers.onChange([])
                        }}
                        checked={fields.status.value === 'client'}
                        id={`radioAddUserClient`}
                        value='client'
                    />
                    <RadioBtn
                        text='Lead'
                        onChange={(e) => {
                            const role = e.target.value
                            setRole(role as UserRole)
                            fields.status.onChange(e.target.value)
                            fields.containers.onChange([])
                        }}
                        checked={fields.status.value === 'lead'}
                        id={`radioAddUserLead`}
                        value='lead'
                    />
                </div>
                <TextArea
                    id='adUserTextArea'
                    variant={"withLabel"}
                    maxLength={200}
                    onChange={(e) => {
                        fields.note.onChange(e.target.value)
                    }}
                    value={fields.note.value}/>

                { role === UserRoles.CLIENT &&
                    <div className={mainStyle.mb24}>
                        <CustomSelectWithCheckboxes
                            onChange={(value: readonly OptionType[],  action: ActionMeta<OptionType>) => {
                                if(value) {
                                    fields.containers.onChange(value as [])
                                }
                            }}
                            name='containers'
                            defaultValue={fields.containers.value}
                            options={containers}
                            menuPlacement={'top'}
                        />
                    </div>
                }


                <Button fontSize={16}>Add user</Button>
            </div>
            </form>
        </div>
    );
};