import React, {createContext, FC, useState} from "react";
import {DeleteModal} from "./DeleteModal";
import {createPortal} from "react-dom";
import style from './Modal.module.scss'
import {AddUserModal} from "./AddUserModal";
import {AddContainerModal} from "./AddContainerModal";



export const MODAL_TYPES = {
    DELETE_USER_MODAL: "DELETE_USER_MODAL",
    ADD_USER_MODAL: "ADD_USER_MODAL",
    ADD_CONTAINER_MODAL: "ADD_CONTAINER_MODAL",
};


const MODAL_COMPONENTS: {[P in typeof MODAL_TYPES[keyof typeof MODAL_TYPES]]: React.FC}= {
    [MODAL_TYPES.DELETE_USER_MODAL]: DeleteModal,
    [MODAL_TYPES.ADD_USER_MODAL]: AddUserModal,
    [MODAL_TYPES.ADD_CONTAINER_MODAL]: AddContainerModal,
};

type ContextType = {
    showModal: (modalType: string, modalProps?: any) => void
    hideModal: () => void
    store: any
};

const initialState: ContextType = {
    showModal: () => {},
    hideModal: () => {},
    store: {}
}

export const GlobalModalContext = createContext(initialState)

export const GlobalModal: React.FC<{children: React.ReactNode}> = ({ children }) => {
    const [store, setStore] = useState<{modalType: keyof typeof MODAL_COMPONENTS| null, modalProps: any}>();
    const { modalType, modalProps } = store || {};

    const showModal = (modalType: string, modalProps: any = {}) => {

        setStore({
            ...store,
            modalType,
            modalProps
        });
    };

    const hideModal = () => {
        setStore({
            ...store,
            modalType: null,
            modalProps: {}
        });
    };

    const renderComponent = () => {
        let ModalComponent;
        if(modalType){
            ModalComponent = MODAL_COMPONENTS[modalType];
        }

        if (!modalType || !ModalComponent) {
            return null;
        }

        return createPortal(<div className={style.modalOverlay}><ModalComponent {...modalProps} /></div>, document.getElementById('modal') as Element | DocumentFragment)
    };

    return (
        <GlobalModalContext.Provider value={{ store, showModal, hideModal }}>
            {renderComponent()}
            {children}
        </GlobalModalContext.Provider>
    );
};
