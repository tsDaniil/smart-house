import {DetailedHTMLProps, FC, InputHTMLAttributes, useState} from "react";
import style from './CheckBox.module.scss'

type CheckBoxPropsType = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    text: string
}

export const CheckBox: FC<CheckBoxPropsType> = ({text, ...props}) => {
    return (
        <label className={style.container}>
            {text}
            <input
                type="checkbox"
                {...props}
            />
            <span className={style.checkmark}/>
        </label>
    )
};