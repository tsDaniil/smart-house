import React, {ChangeEvent, FC, useEffect, useLayoutEffect, useRef, useState,} from "react";
import style from './TextArea.module.scss'
import mainStyle from '../../styles/Main.module.scss'
import cn from "classnames";

type TextAreaPropsType = React.DetailedHTMLProps<React.TextareaHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement> & {
    variant?: 'withLabel'
}
export const TextArea: FC<TextAreaPropsType> = ({variant='general', ...props}) => {
    const [minHeight, setMinHeight] = useState<number | null>(null);
    const textAreaRef = useRef<HTMLTextAreaElement>(null);


    const changeHandler = (e: ChangeEvent<HTMLTextAreaElement>) => {
        const height = textAreaRef.current!.scrollHeight;
        if(minHeight && height > minHeight) {
            textAreaRef.current!.style.height = 'auto';
            textAreaRef.current!.style.height = `${textAreaRef.current!.scrollHeight}px`;
        }
    }

    useEffect(()=>{
        setMinHeight(textAreaRef.current!.clientHeight)
        textAreaRef.current!.style.height = `${textAreaRef.current!.scrollHeight}px`;
    },[])

    return (
        <>
        <div className={cn(mainStyle.height100, {[style.textAreaWrapper]: variant === 'withLabel'})}>
            {
                variant === 'withLabel' && <div className={style.textAreaLabel}>Note</div>
            }

            <textarea
                ref={textAreaRef}
                onChange={changeHandler}
                className={cn(style.textarea,style[variant])}
                {...props}
            />
        </div>
    </>
    );
};