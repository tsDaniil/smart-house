import style from './InfoBlock.module.scss'
import {FC} from "react";

type BadgePropsType = {
    Icon: FC<{ iconClass: string }>
    text: string
    value: number
}

export const InfoBlock: FC<BadgePropsType> = ({Icon, text, value}) => {
    return (
        <div className={style.badge}>
            <Icon iconClass={style.icon}/>
            <div>
                <div className={style.text}>{text}</div>
                <div className={style.value}>{value}</div>
            </div>
        </div>
    );
};