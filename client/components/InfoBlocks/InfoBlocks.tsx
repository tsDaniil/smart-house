import {FC} from "react";
import {InfoBlock} from "./InfoBlock";
import {LeadsIcon} from "./Icons/LeadsIcon";
import {UsersIcon} from "./Icons/UsersIcon";
import {ContainersIcon} from "./Icons/ContainersIcon";
import style from './InfoBlocks.module.scss'

export const InfoBlocks: FC<{  }> = props => {
    return (
        <div className={style.badges}>
            <InfoBlock Icon={LeadsIcon} text={'Leads'} value={10}/>
            <InfoBlock Icon={UsersIcon} text={'Users'} value={94}/>
            <InfoBlock Icon={ContainersIcon} text={'Containers'} value={368}/>
        </div>
    );
};