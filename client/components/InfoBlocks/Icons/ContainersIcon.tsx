import {FC} from "react";

export const ContainersIcon: FC<{ iconClass: string }> = ({iconClass}) => {
    return (
        <svg className={iconClass} width="48" height="48" viewBox="0 0 48 48" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <rect width="48" height="48" rx="8" fill="#ECF0FF"/>
            <path fillRule="evenodd" clipRule="evenodd"
                  d="M16 14C14.8954 14 14 14.8954 14 16V32C14 33.1046 14.8954 34 16 34H20H21V33V15V14H20H16ZM16 16L19 16V32H16V16ZM24 14H23V15V33V34H24H32C33.1046 34 34 33.1046 34 32V16C34 14.8954 33.1046 14 32 14H24ZM25 32V16H32V32H25Z"
                  fill="#2D5BFF"/>
            <path d="M19 16L16 16V32H19V16Z" fill="#2D5BFF"/>
            <path d="M25 16V32H32V16H25Z" fill="#2D5BFF"/>
        </svg>
    );
};


