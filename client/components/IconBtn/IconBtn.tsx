import {FC} from "react";
import style from './IconBtn.module.scss'
import cn from "classnames";

type IconBtnPropsType = {
    color?: 'orange' | 'blue'
    Icon: FC<{ iconClass: string }>
    clickHandler:() => void
}

export const IconBtn: FC<IconBtnPropsType> = ({color = 'orange', Icon, clickHandler}) => {
    const onClick = (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation()
        clickHandler()
    }
    return (
        <div onClick={onClick} className={cn(style.btn, style[color])}>
            <Icon iconClass={style.icon}/>
        </div>
    );
};