import style from './Badge.module.scss'
import {FC, ReactNode} from "react";
import cn from "classnames";
import {UserRolesColors} from "../../types/types";

type BadgePropsType = {
    children: ReactNode
    backgroundColor: UserRolesColors
}

export const Badge: FC<BadgePropsType> = ({children, backgroundColor}) => {
    return (
        <div className={cn(style.badge, style[backgroundColor])}>
            {children}
        </div>
    );
};