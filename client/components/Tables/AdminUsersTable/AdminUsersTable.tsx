import React, {FC, useContext} from "react";
import style from './AdminUsersTable.module.scss'
import mainStyle from '../../../styles/Main.module.scss'
import tableStyle from "../Table.module.scss";
import cn from "classnames";
import {GlobalModalContext, MODAL_TYPES} from "../../Modals/GlobalModal";
import {Table} from "../Table";
import {Cell} from "../Cell";
import {Badge} from "../../Badge/Badge";
import {UserRolesColors, UsersType} from "../../../types/types";
import {IconBtn} from "../../IconBtn/IconBtn";
import {DeleteIcon} from "../Icons/DeleteIcon";
import {RowWithTable} from "./RowWithTable";
import {USERS_TABLE_HEADERS} from "../../../constants/CONSTANTS";
import {useStore} from "effector-react";
import {$users} from "../../../admin/model";


export const usersMock: UsersType[] = [
    {id:1, name: 'Adam Kronenberg', email: 'eukanuba@bluewhalecorp.com', status: 'lead', date: '24 may 2022', note: 'Ehmmm... I want to buy stuff. U know. The big one. Two of them actualy... or three.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}], containers: []},
    {id:2, name: 'Jacob', email: 'jacobw1984@gmail.com', status: 'lead', date: '24 jan 2022', note: 'Rich dude. Special treatment required.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}], containers: []},
    {id:3, name: 'Adamssssss', email: 'awachowsky@yahoo.com', status: 'lead', date: '23 may 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}],  containers: []},
    {id:4, name: 'Jack Ma', email: 'thema@alibabagroup.com', status: 'client', date: '20 may 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.',interests:[], containers: [{serial: 123456, model: 'ASI-C40HQ'}, {serial: 654321, model: 'ASI-C20HQ'}, {serial: 1236544, model: 'ASI-C20HQ'}],},
    {id:5, name: 'Roger Moore', email: 'galina69@mail.ru', status: 'lead', date: '28 apr 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}], containers: []},
    {id:6, name: 'Heisenberg', email: 'ww@breakingbad.io', status: 'client', date: '27 apr 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.',interests:[], containers: [{serial: 123456, model: 'ASI-C40HQ'}, {serial: 654321, model: 'ASI-C20HQ'}, {serial: 1236544, model: 'ASI-C20HQ'}],},
    {id:7, name: 'Jannies Wu', email: 'bigredwu@china.org', status: 'lead', date: '25 apr 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}], containers: []},
    {id:8, name: 'Karl Marx', email: 'comunism@newbrightideas.eu', status: 'lead', date: '22 mar 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}], containers: []},
    {id:9, name: '1111111', email: 'randommail@unknown.org', status: 'lead', date: '20 mar 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.', interests: [{model: 'ASI-C40HQ', qty: 'Qty', amount: '3'},{model: 'ASI-T20M', qty: 'Qty', amount: '3'},{model: 'ASI-T30M', qty: 'Qty', amount: '3'}], containers: []},
    {id:10, name: 'Baikal Muhamedov', email: 'baikal@baikalmining.ru', status: 'client', date: '20 mar 2022', note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet eleifend risus at tempus.',interests:[], containers: [{serial: 123456, model: 'ASI-C40HQ'}, {serial: 654321, model: 'ASI-C20HQ'}, {serial: 1236544, model: 'ASI-C20HQ'}],},
]

export const AdminUsersTable: FC<{  }> = props => {
    const { showModal } = useContext(GlobalModalContext);
    const users = useStore($users)
    const createDeleteModal = () => {
        showModal(
            MODAL_TYPES.DELETE_USER_MODAL,
            {
                modalTitle: 'Do you want delete user?',
            }
        )
    };
    return (
        <Table tableHeaders={USERS_TABLE_HEADERS}  addStyle={tableStyle.usersAdmin}>
            {
                usersMock.map( user => {
                    return (
                        <RowWithTable  key={user.id} createDeleteModal={createDeleteModal} user={user}>
                            {
                                Object.keys(USERS_TABLE_HEADERS).map(prop => {
                                    return (
                                        <Cell key={`${prop}-${user.id}`}>
                                            {
                                                prop !== 'status' && prop !== 'actions' && prop !== 'email'
                                                    ? <>{user[prop as keyof typeof user]}</>
                                                    : prop === 'status'
                                                    ? <div className={cn(mainStyle.flexRowCenterCenter, mainStyle.w100)}><Badge backgroundColor={UserRolesColors[user[prop] as keyof typeof UserRolesColors]}>
                                                        {user[prop]}
                                                    </Badge></div>
                                                    : prop === 'email'
                                                    ? <a
                                                            className={style.emailLink}
                                                            href={`mailto:${user[prop as keyof typeof user]}`}
                                                            onClick={(e)=> {e.stopPropagation()}}
                                                        >
                                                            {user[prop as keyof typeof user]}
                                                    </a>
                                                    : <div className={cn(mainStyle.flexRowJustifyEnd, mainStyle.w100)}><IconBtn clickHandler={()=>{createDeleteModal()}} Icon={DeleteIcon}/></div>
                                            }
                                        </Cell>
                                    )
                                })
                            }
                        </RowWithTable>
                    )
                })
            }
        </Table>
    );
};