import {Row} from "../Row";
import mainStyle from "../../../styles/Main.module.scss";
import {UsersType} from "../../../types/types";
import style from "./AdminUsersTable.module.scss";
import {DropDownTable} from "./DropDownTable";
import React, {ReactNode, useState} from "react";

type RowWithTablePropsType = {
    user: UsersType
    createDeleteModal: () => void
    children: ReactNode
}

export const RowWithTable: React.FC<RowWithTablePropsType> = ({user, createDeleteModal, children}) => {
    const [isEditing, setIsEditing] = useState<boolean>(false);
    const toggleEdit = () => {
        setIsEditing(actual => !actual)
    }
    return (
        <>
            <Row onClick={toggleEdit}>
                {children}
            </Row>
            {
                isEditing && <tr className={style.expandableRow}>
                    <td colSpan={5} className={mainStyle.relativePos}>
                        <DropDownTable user={user} editOf={toggleEdit}/>
                    </td>
                </tr>
            }
        </>
    );
};