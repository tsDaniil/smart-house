import style from "./DropDownTable.module.scss";
import mainStyle from '../../../styles/Main.module.scss'
import table from '../Table.module.scss'
import React, {ChangeEvent, useEffect, useState} from "react";
import {UserRole, UsersType} from "../../../types/types";
import {containersMock} from "../../../pages";
import {CustomSelect, OptionType} from "../../CustomSelect/CustomSelect";
import {Row} from "../Row";
import {Table} from "../Table";
import {RadioBtn} from "../../RadioButton/RadioBtn";
import {Button} from "../../Button/Button";
import {CheckIcon} from "../../Button/Icons/CheckIcon";
import {TextArea} from "../../TextArea/TextArea";
import cn from "classnames";
import {ActionMeta} from "react-select/dist/declarations/src/types";
import {CustomSelectWithCheckboxes} from "../../CustomSelect/CustomSelectWithCheckboxes";

type DropDownTablePropsType = {
    user: UsersType
    editOf: () => void
}
export const DROPDOWN_TABLE_HEADERS = {
    client: {
        note: 'Note',
        interests: 'Containers',
        statusActions: 'Status',
    },
    lead: {
        note: 'Note',
        interests: 'Interested in',
        statusActions: 'Status',
    }
}

export const DropDownTable: React.FC<DropDownTablePropsType> = ({user, editOf}) => {
    const [role, setRole] = useState<UserRole | null>(null);
    useEffect(() => setRole(user.status), [])

    const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        console.log(e.target.value)
        setRole(e.target.value as UserRole)
    }
    const saveHandler = () => {
        editOf()
    }
    const userRole = user.status
    const tableHeaders = userRole === 'client' ? DROPDOWN_TABLE_HEADERS.client : DROPDOWN_TABLE_HEADERS.lead
    const containers = containersMock.map(container => ({value: container.id, label: container.serial}))
    const userContainersColumn =
        userRole === 'client'
            ? <div className={mainStyle.darkGrayText}>
                {
                    user.containers.map(container => {
                        return (
                            <div key={container.serial}>
                                <span
                                    className={cn(mainStyle.fontWeight700, mainStyle.pl8, mainStyle.pr8)}>{container.serial}</span>{container.model}
                            </div>
                        )
                    })
                }
                <div className={mainStyle.mt8}>
                    {/*TODO при открытии селекта внизу страницы - ему не хватает места и он ломает верстку*/}
                    <CustomSelectWithCheckboxes
                        onChange={(value: readonly OptionType[], action: ActionMeta<OptionType>) => {
                            console.log(value, action)
                        }}
                        defaultValue={{value: '', label: ''}}
                        name='containers'
                        options={containers}
                        menuPortalTarget={document.body}
                    />
                </div>
            </div>
            : <div className={mainStyle.darkGrayText}>
                {
                    user.interests?.map(interest => {
                        return (
                            <div key={interest.model}>{`${interest.model} ${interest.qty} ${interest.amount}`}</div>
                        )
                    })
                }
            </div>
    return (
        <>
            <div className={style.dropdownOuter}>
                <div className={style.dropdownInner}/>
            </div>
            <Table tableHeaders={tableHeaders} addStyle={table.dropDownTable}>
                <Row>
                    {
                        Object.keys(tableHeaders).map(prop => {
                            let column;
                            switch (prop) {
                                case 'statusActions':
                                    column = <div className={style.changeRole}>
                                        <div className={style.radioGroup}>
                                            <RadioBtn
                                                onChange={changeHandler}
                                                checked={role === 'client'}
                                                text='Client'
                                                id={`${user.id}-radio-client`}
                                                value='client'
                                            />
                                            <RadioBtn
                                                onChange={changeHandler}
                                                checked={role === 'lead'}
                                                text='Lead'
                                                id={`${user.id}-radio-lead`}
                                                value='lead'
                                            />
                                        </div>
                                        <div className={style.saveBtn}>
                                            <Button textWeight={800} fontSize={16} onClick={saveHandler}>
                                                <CheckIcon/>
                                                &nbsp;
                                                Save
                                            </Button>
                                        </div>
                                    </div>
                                    break
                                case 'note':
                                    column = <TextArea
                                        value={'Ehmmm... I want to buy stuff. U know. The big one. Two of them actualy... or three.'}
                                    />
                                    break
                                case 'interests':
                                    column = userContainersColumn
                            }
                            return (
                                <td key={`${prop}-${user.id}`} className={style.actions}>
                                    {column}
                                </td>
                            )
                        })
                    }
                </Row>
            </Table>
        </>
    );
};