import {FC, ReactNode} from "react";
import style from './Table.module.scss'
import mainStyle from '../../styles/Main.module.scss'

type CellPropsType = React.DetailedHTMLProps<React.TdHTMLAttributes<HTMLTableDataCellElement>, HTMLTableDataCellElement> & {
    children: ReactNode
}
export const Cell: FC<CellPropsType> = ({children, ...props}) => {
    return (
        <td className={style.cell} {...props}>
            <div className={mainStyle.flexRowCenter}>
                {children}
            </div>
        </td>
    );
};