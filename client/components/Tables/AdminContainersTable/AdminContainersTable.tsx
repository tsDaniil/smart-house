import React, {FC} from "react";
import {RowWithEditMode} from "./RowWithEditMode";
import {ContainerType} from "../../../types/types";
import {Table} from "../Table";
import table from '../Table.module.scss'
import {ADMIN_CONTAINERS_TABLE_HEADERS} from "../../../constants/CONSTANTS";

type UserContainersTablePropsType = {
    tableHeaders: typeof ADMIN_CONTAINERS_TABLE_HEADERS
    containers: ContainerType[]
}

export const AdminContainersTable: FC<UserContainersTablePropsType> = ({tableHeaders, containers}) => {

    return (
        <Table tableHeaders={tableHeaders} addStyle={table.adminContainers}>
            {
                containers.map(container => {
                    return (
                        <RowWithEditMode key={container.id} container={container}/>
                    )
                })
            }
        </Table>
    );
};