import React, {FC, useContext, useState} from "react";
import mainStyle from "../../../styles/Main.module.scss";
import cn from "classnames";
import style from "./AdminContainersTable.module.scss";
import {ContainerType, InputSize} from "../../../types/types";
import {IconBtn} from "../../IconBtn/IconBtn";
import {CheckIcon} from "../../Button/Icons/CheckIcon";
import {DeleteIcon} from "../Icons/DeleteIcon";
import {CustomSelect, OptionType} from "../../CustomSelect/CustomSelect";
import {TextField} from "../../TextField/TextField";
import {GlobalModalContext, MODAL_TYPES} from "../../Modals/GlobalModal";
import {ActionMeta} from "react-select/dist/declarations/src/types";

type StrokePropsType = React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableRowElement>, HTMLTableRowElement> & {
    container: ContainerType
}

export const RowWithEditMode: FC<StrokePropsType> = ({container}) => {
    const [isEditMode, setIsEditMode] = useState(false);
    const { showModal } = useContext(GlobalModalContext);
    const models = [
        {value: 'ASI-C40HQ', label: 'ASI-C40HQ'},
        {value: 'ASI-T20M', label: 'ASI-T20M'},
        {value: 'ASI-T30M', label: 'ASI-T30M'},
        {value: 'ASI-C20HQ', label: 'ASI-C20HQ'},
    ]
    const editOn = () => {
        setIsEditMode(actual => !actual)
    }
    const editOf = () => {
        setIsEditMode(false)
    }
    const createDeleteModal = () => {
        showModal(
            MODAL_TYPES.DELETE_USER_MODAL,
            {
                modalTitle: 'Do you want delete container?',
            }
        )
    };
    return (
        <tr onClick={() =>editOn()} className={cn({[style.isEditing]: isEditMode})}>
            {
                ([...Object.keys(container), 'actions'] as (keyof ContainerType | 'actions')[]).map(prop => {
                    if(prop === 'id' || prop === 'status') return false
                    return (
                        <td key={`${prop}-${container.id}`}>
                            {
                                prop === 'actions'
                                    ? <div className={mainStyle.flexRowJustifyEnd}>
                                        {isEditMode && <IconBtn clickHandler={editOf} Icon={CheckIcon} color={"blue"}/>}
                                        <IconBtn clickHandler={()=>{createDeleteModal()}} Icon={DeleteIcon}/>
                                    </div>
                                        : isEditMode
                                            ? prop === 'model'
                                                ? <div onClick={(e:React.MouseEvent) => e.stopPropagation()}><CustomSelect
                                                        onChange={(value: readonly OptionType[] | OptionType | null, action: ActionMeta<OptionType>) =>{
                                                            console.log(value, action)
                                                        }}
                                                        name='model'
                                                        defaultValue={{value: container[prop], label: container[prop]}}
                                                        options={models}
                                                    /></div>
                                                : <div className={mainStyle.tableCellHeight}><TextField
                                                type='text'
                                                name={prop}
                                                inputSize={InputSize.SM}
                                                fontSize={16}
                                                value={container[prop]}
                                                onClick={(e:React.MouseEvent) => e.stopPropagation()}
                                            /></div>
                                    : <div className={mainStyle.flexRowCenter}>{container[prop]}</div>
                            }
                        </td>
                    )
                })
            }
        </tr>
    )
};