import React, {FC} from "react";
import {useRouter} from "next/router";
import tableStyle from '../Table.module.scss'
import {ContainerType, StatusesColors} from "../../../types/types";
import {Table} from "../Table";
import {Row} from "../Row";
import {Cell} from "../Cell";
import {Dot} from "../../Dot/Dot";
import {CONTAINERS_TABLE_HEADERS, DOT_COLORS} from "../../../constants/CONSTANTS";

type UserContainersTablePropsType = {
    tableHeaders: typeof CONTAINERS_TABLE_HEADERS
    containers: ContainerType[]
}

export const UserContainersTable: FC<UserContainersTablePropsType> = ({tableHeaders, containers}) => {
    const router = useRouter()
    const onSelectContainer = (id: number) => {
        router.push(`/containers/${id}`)
    }

    return (
        <Table tableHeaders={tableHeaders} addStyle={tableStyle.containersUser}>
            {
                containers.map(container => {
                    return (
                        <Row onClick={onSelectContainer.bind(this, container.id)} key={container.id}>
                            {
                                (Object.keys(container) as (keyof ContainerType)[]).map(prop => {
                                    if(prop === 'id') return false
                                    return (
                                        <Cell key={`${prop}-${container.id}`}>
                                            {
                                                prop === 'status' && <Dot
                                                    color={DOT_COLORS[container[prop] as keyof typeof DOT_COLORS] as StatusesColors}
                                                    size={8}
                                                    spacing={4}
                                                />
                                            }
                                            {container[prop]}
                                        </Cell>
                                    )
                                })
                            }
                        </Row>
                    )
                })
            }
        </Table>
    );
};