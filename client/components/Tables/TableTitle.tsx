import {FC, ReactNode} from "react";
import style from './TableTitle.module.scss'
import {Dot} from "../Dot/Dot";
import {StatusesColors} from "../../types/types";

type TableTitlePropsType = {
    children: ReactNode
    dotColor: StatusesColors
}

export const TableTitle: FC<TableTitlePropsType> = ({children, dotColor}) => {
    return (
        <div className={style.tableTitle}>
            <Dot color={dotColor}/>
            <h2>{children}</h2>
        </div>
    );
};