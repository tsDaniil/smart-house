import {FC} from "react";
import style from './ArrowIcon.module.scss'

type ArrowIconPropsType = {
    className: string
}

export const ArrowIcon: FC<ArrowIconPropsType> = ({className}) => {
    return (
        <svg className={style[className]} width="8" height="12" viewBox="0 0 8 12" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd"
                  d="M1.29966 3.40058C0.95455 3.65525 0.451109 3.60347 0.175192 3.28494C-0.100725 2.9664 -0.0446328 2.50173 0.300476 2.24705L2.83429 0.377243C3.5159 -0.125748 4.4841 -0.125748 5.16571 0.377243L7.69952 2.24705C8.04463 2.50173 8.10072 2.9664 7.82481 3.28494C7.54889 3.60347 7.04545 3.65525 6.70034 3.40058L4.80004 1.99826L4.80004 11.2616C4.80004 11.6694 4.44185 12 4 12C3.55815 12 3.19996 11.6694 3.19996 11.2616L3.19996 1.99826L1.29966 3.40058Z"
                  fill="#2D5BFF"/>
        </svg>

    );
};