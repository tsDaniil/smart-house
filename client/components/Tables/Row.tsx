import React, {FC, ReactNode, useState} from "react";

type StrokePropsType = React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableRowElement>, HTMLTableRowElement> & {
    children: ReactNode
}

export const Row: FC<StrokePropsType> = ({children, ...props}) => {
    return (
        <tr {...props}>
            {children}
        </tr>
    );
};