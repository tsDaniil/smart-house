import React, {ChangeEvent, FC, useState} from "react";
import {SearchIcon} from "./Icons/SearchIcon";
import {TextFieldWithIcon} from "../TextFieldWithIcon/TextFieldWithIcon";

export const TableSearch: FC<{}> = props => {
    const [searchTerm, setSearchTerm] = useState('');

    const changeFormHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(e.target.value)
    }

    return (
        <TextFieldWithIcon
            Icon={SearchIcon}
            type={'text'}
            name={'search'}
            placeholder={'Containers Search'}
            value={searchTerm}
            onChange={changeFormHandler}
        />
    );
};