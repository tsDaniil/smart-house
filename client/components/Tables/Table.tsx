import {FC, ReactNode, useState} from "react";
import styles from './Table.module.scss'
import cn from "classnames";
import {ArrowIcon} from "./Icons/ArrowIcon";
import {useRouter} from "next/router";

type TablePropsType = {
    tableHeaders: {}
    children: ReactNode
    addStyle?: string
}
type SortType = {
    sortKey: string
    isRevers: boolean | null
}
export const Table: FC<TablePropsType> = ({tableHeaders, children, addStyle}) => {
    const [sortType, setSortType] = useState<SortType>({
        sortKey: 'none',
        isRevers: null
    });
    const router = useRouter()
    const onSort = (filterName: string) => {
        setSortType((prevState) => {
            let isSortRevers;
            if ((prevState.sortKey === filterName) && prevState.isRevers) {
                isSortRevers = null
            } else {
                isSortRevers = prevState.sortKey === filterName && !prevState.isRevers
            }
            const newFilterName = (prevState.sortKey === filterName && prevState.isRevers) ? 'none' : filterName
            console.log({sortKey: newFilterName, isRevers: isSortRevers})
            return {sortKey: newFilterName, isRevers: isSortRevers}
        })
    }
    const clickHandler = (id: number) => {
        router.push(`/containers/${id}`)
    }
    return (
        <table className={cn(styles.table, addStyle)}>
            <thead>
            <tr>
                {
                    Object.keys(tableHeaders).map((prop) => {
                        return (
                            <th
                                key={prop}
                                className={cn(styles.th, styles[prop])}
                                onClick={() => onSort(prop)}
                            >
                                {tableHeaders[prop as keyof typeof tableHeaders]}
                                {
                                    prop === sortType.sortKey
                                    && <ArrowIcon className={sortType.isRevers ? 'arrowIcon' : 'reverseArrowIcon'}/>
                                }
                            </th>
                        )
                    })
                }
            </tr>
            </thead>
            <tbody>
                {children}
            </tbody>
        </table>
    );
};