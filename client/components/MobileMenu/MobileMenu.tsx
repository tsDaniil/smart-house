import style from "./MobileMenu.module.scss";
import main from '../../styles/Main.module.scss'
import React, {useEffect, useLayoutEffect, useRef, useState} from "react";
import cn from "classnames";
import {Menu} from "../SideBar/Menu";
import {User} from "../User/User";

export const MobileMenu: React.FC<{  }> = props => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const mobileMenu = useRef<HTMLDivElement>(null)

    useEffect(() => {
        const onClick = ({target}:MouseEvent):void => {
            if(!!mobileMenu.current && !mobileMenu.current.contains(target as Node) && isOpen) {
                console.log('close menu')
                setIsOpen(false)
            }
        }
        document.addEventListener('click', onClick)
        return () => document.removeEventListener('click', onClick)
    }, [isOpen]);

    const openMenuHandler = () => {
        setIsOpen(actual => !actual)
    }
    return (
        <div ref={mobileMenu} className={main.block}>
            <div className={cn(style.menuIcon, main.flexColumnBetween)} onClick={openMenuHandler}>
                <span/>
                <span/>
                <span/>
            </div>
            {
                isOpen && <div className={style.mobileMenu}>
                    <Menu/>
                    <User/>
                </div>
            }

        </div>
    );
};