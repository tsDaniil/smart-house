import {FC} from "react";
import style from './Dot.module.scss'
import mainStyle from '../../styles/Main.module.scss'
import cn from "classnames";
import {StatusesColors} from "../../types/types";

type DotPropsType = {
    color: StatusesColors
    size?: number
    spacing?: number
    positioned?: boolean
}

export const Dot: FC<DotPropsType> = ({
          color, size = 12,
          spacing = 8, positioned
      }) => {
    return (
        <div
            className={cn(style.dot, mainStyle[color], positioned && style.positioned)}
            style={{width: size, height: size, marginRight: spacing}}
        />
    )
};