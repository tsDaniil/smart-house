import {DetailedHTMLProps, FC, InputHTMLAttributes} from "react";
import style from './RadioBtn.module.scss'

type RadioBtnPropsType = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    text: string
}

export const RadioBtn: FC<RadioBtnPropsType> = ({text, ...props}) => {
    return (
        <>
            <input className={style.radio} type="radio" {...props} />
            <label htmlFor={props.id} className={style.radioLabel}>{text}</label>
        </>
    );
};