import {FC} from "react";
import textField from '../TextField/TextField.module.scss'
import style from './TextFieldWithIcon.module.scss'
import {TextField, TextFieldPropsType} from "../TextField/TextField";

type TextFieldWithIconPropsType = TextFieldPropsType & {
    Icon: FC<{ iconClass: string }>
    st?: string[]
};

export const TextFieldWithIcon: FC<TextFieldWithIconPropsType> = ({Icon,st=[], ...props}) => {
    return (
        <TextField {...props} st={[...st, textField.withIcon]}>
            <Icon iconClass={style.icon}/>
            {props.children}
        </TextField>
    );
};

