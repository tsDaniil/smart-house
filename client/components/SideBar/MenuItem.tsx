import Link from "next/link";
import {FC} from "react";
import {useRouter} from "next/router";
import style from './MenuItem.module.scss'
import {Dot} from "../Dot/Dot";
import {StatusesColors} from "../../types/types";

type MenuItemPropsType = {
    text: string
    href: string
    Icon: FC<{ iconClass: string }>
    hasUpdate: boolean
}

export const MenuItem: React.FC<MenuItemPropsType> = ({text, href, Icon, hasUpdate}) => {
    const router = useRouter()
    const iconClass = `icon ${href === router.pathname ? 'iconActiveColor' : 'iconDefaultColor'}`
    return (
        <Link href={href}>
            <a className={href === router.pathname ? style.activeNavItem : style.navItem}>
                {hasUpdate && <Dot color={StatusesColors.RED} size={8} positioned/>}
                <Icon iconClass={iconClass}/>
                {text}
            </a>
        </Link>
    );
};