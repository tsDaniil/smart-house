import style from './Menu.module.scss';
import {FC} from "react";
import {MenuItem} from './MenuItem';
import {NAV_ITEMS} from "../../constants/CONSTANTS";

export const Menu: FC<{}> = () => {
    return (
        <nav className={style.navigation}>
            <ul>
                {NAV_ITEMS.map(navItem => <li key={navItem.text}><MenuItem {...navItem}/></li>)}
            </ul>
        </nav>
    )
}
