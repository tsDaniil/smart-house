import React from "react";
import {IconPropsType} from "../../../types/types";

export const ContainersIcon: React.FC<IconPropsType> = ({iconClass}) => {
    return (
        <svg className={iconClass} width="32" height="32" viewBox="0 0 32 32" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path className='background'
                  d="M0 8C0 3.58172 3.58172 0 8 0H24C28.4183 0 32 3.58172 32 8V24C32 28.4183 28.4183 32 24 32H8C3.58172 32 0 28.4183 0 24V8Z"/>
            <path className='line' fillRule="evenodd" clipRule="evenodd"
                  d="M10.6667 9.33337C9.93029 9.33337 9.33334 9.93033 9.33334 10.6667V21.3334C9.33334 22.0698 9.93029 22.6667 10.6667 22.6667H13.3333H14V22V10V9.33337H13.3333H10.6667ZM10.6667 10.6667L12.6667 10.6667V21.3334H10.6667V10.6667ZM16 9.33337H15.3333V10V22V22.6667H16H21.3333C22.0697 22.6667 22.6667 22.0698 22.6667 21.3334V10.6667C22.6667 9.93033 22.0697 9.33337 21.3333 9.33337H16ZM16.6667 21.3334V10.6667H21.3333V21.3334H16.6667Z"/>
        </svg>
    );
};