import React from 'react';
import {Menu} from './Menu';
import sidebar from './Sidebar.module.scss';
import {ExitIcon} from "./MenuIcons/ExitIcon";
import {useRouter} from "next/router";
import {LogoIcon} from "./Icons/LogoIcon";
import { logoutFx } from '../../models/auth';

export const SideBar = () => {
    const router = useRouter();
    const onClickHandler = () => logoutFx()
        .then(() => router.replace('/authorization'));
    return (
        <div className={sidebar.sidebar}>
            <div>
                <div className={sidebar.header}>
                    <LogoIcon iconClass={sidebar.logo}/>
                </div>
                <Menu/>
            </div>

            <div onClick={onClickHandler} className={sidebar.exitBtn}>
                <ExitIcon iconClass='icon iconDefaultColor'/>
                Exit
            </div>
        </div>
    )

}