import {TextField} from "../TextField/TextField";
import style from '../AuthorizationForm/AuthorizationForm.module.scss'
import {Button} from "../Button/Button";
import {ChangeEvent, FormEvent, useState} from "react";
import {EmailIcon} from "./Icons/EmailIcon";
import {InputSize} from "../../types/types";
import {TextFieldWithIcon} from "../TextFieldWithIcon/TextFieldWithIcon";

export const ForgotPasswordForm: React.FC<{}> = props => {
    const [email, setEmail] = useState('');

    const changeFormHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setEmail(e.target.value)
    }

    const submitHandler = (e: FormEvent) => {
        e.preventDefault()
        console.log(email)
    }

    return (
        <form onSubmit={submitHandler} className={style.form}>
            <div className={style.field}>
                <TextFieldWithIcon
                    Icon={EmailIcon}
                    type='email'
                    name='email'
                    placeholder='e-mail'
                    inputSize={InputSize.LG}
                    value={email}
                    onChange={changeFormHandler}
                />
            </div>

            <Button type={'submit'}>Recover Password</Button>

        </form>
    );
};