import {FC} from "react";

export const EmailIcon: FC<{ iconClass: string }> = ({iconClass}) => {
    return (
        <svg className={iconClass} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <rect width="50" height="50" fill="#EDEDED"/>
            <path fillRule="evenodd" clipRule="evenodd"
                  d="M19.8148 19C18.8125 19 18 19.8679 18 20.9385V29.0615C18 30.1321 18.8125 31 19.8148 31H30.1852C31.1875 31 32 30.1321 32 29.0615V20.9385C32 19.8679 31.1875 19 30.1852 19H19.8148ZM19.5556 21.5988V29.0615C19.5556 29.2145 19.6716 29.3385 19.8148 29.3385H30.1852C30.3284 29.3385 30.4444 29.2145 30.4444 29.0615V21.5988L26.1024 25.1454C25.4517 25.6769 24.5483 25.6769 23.8976 25.1454L19.5556 21.5988ZM29.0311 20.6615H20.9689L24.8425 23.8255C24.9355 23.9015 25.0645 23.9015 25.1575 23.8255L29.0311 20.6615Z"
                  fill="#7C7B7B"/>
        </svg>

    );
};


