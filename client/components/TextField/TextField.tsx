import {DetailedHTMLProps, FC, HTMLInputTypeAttribute, InputHTMLAttributes, useState} from "react";
import style from './TextField.module.scss'
import cn from "classnames";
import {InputSize} from "../../types/types";

export type TextFieldPropsType = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    type?: HTMLInputTypeAttribute
    inputSize?: InputSize
    st?: string[]
    fontSize?: number
};

export const TextField: FC<TextFieldPropsType> = ({
                          inputSize = InputSize.MD,
                          children,
                          st = [], fontSize,
                          ...props
                      }) => {
    return (
        <div
            className={cn(style.filed, style[inputSize], style[`fontSize${fontSize}`], ...st)}
        >
            <input
                {...props}
            />

            <div className={style.outline}/>
            {children}
        </div>
    );
};

