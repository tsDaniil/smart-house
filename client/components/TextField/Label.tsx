import {ReactNode} from "react";
import style from './Label.module.scss'

type LabelPropsType = React.DetailedHTMLProps<React.LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement> & {
    children: ReactNode
}

export const Label: React.FC<LabelPropsType> = props => {
    return (
        <label {...props} className={style.label}>
            {props.children}
        </label>
    );
};