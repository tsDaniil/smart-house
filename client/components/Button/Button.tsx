import style from "./Button.module.scss";
import {ButtonHTMLAttributes, FC} from "react";
import cn from "classnames";
import {ButtonColorThemes, ButtonVariant} from "../../types/types";

type ButtonPropsType = {
    variant?: ButtonVariant
    textWeight?: 800 | 700
    fontSize?: number
    colorTheme?: ButtonColorThemes

} & ButtonHTMLAttributes<HTMLButtonElement>;

export const Button: FC<ButtonPropsType> = ({
                                                variant='',
                                                colorTheme = ButtonColorThemes.blue,
                                                textWeight,
                                                fontSize,
                                                ...props
                                            }) => {
    return (

        <button
            {...props}
            className={cn(style.button, style[variant], style[colorTheme], style[`font${fontSize}`])}
            style={{
                fontWeight: textWeight,
            }}
        >
            {props.children}
        </button>
    );
};