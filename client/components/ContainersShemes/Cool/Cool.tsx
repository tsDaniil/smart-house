import cn from "classnames";
import scheme from "../ContainerScheme.module.scss";
import style from './Cool.module.scss'
import {StatusesColors} from "../../../types/types";
import {TemperatureIncomingIcon} from "../Icons/TemperatureIncomingIcon";
import {TemperatureOutcomingIcon} from "../Icons/TemperatureOutcomingIcon";
import {CoolerIcon} from "../Icons/CoolerIcon";

type CoolPropsType = {
    color: StatusesColors
    tempIncoming: number | null
    tempOutcoming: number | null
    coolName: string
}


export const Cool: React.FC<CoolPropsType> = ({color, tempIncoming, tempOutcoming, coolName}) => {
    return (
        <div className={cn(scheme.scheme, style.cool, scheme[color])}>
            <div>
                <TemperatureIncomingIcon iconClass={style.incomingTemp}/>
                <div>T<sup>out</sup>: <b>+{tempIncoming}</b></div>
            </div>
            <div>
                <h6>{coolName}</h6>
                <div>
                    <div className={style.coolers}>
                        <div>
                            <CoolerIcon iconClass={scheme.coolerIcon}/>
                            <div>RPM: <b>1200</b></div>
                        </div>
                        <div>
                            <CoolerIcon iconClass={scheme.coolerIcon}/>
                            <div>RPM: <b>1200</b></div>
                        </div>
                    </div>
                    <div className={style.coolers}>
                        <div>
                            <CoolerIcon iconClass={scheme.coolerIcon}/>
                            <div>RPM: <b>1200</b></div>
                        </div>
                        <div>
                            <CoolerIcon iconClass={scheme.coolerIcon}/>
                            <div>RPM: <b>1200</b></div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <TemperatureOutcomingIcon iconClass={style.outcomingTemp}/>
                <div>T<sup>in</sup>: <b>+{tempOutcoming}</b></div>
            </div>
        </div>
    );
};