import cn from "classnames";
import scheme from "../ContainerScheme.module.scss";
import style from './Pump.module.scss'
import {StatusesColors} from "../../../types/types";
import {PumpIcon} from "../Icons/PumpIcon";

type PumpPropsType = {
    color: StatusesColors
    preasure: number
    rpm: number
    orderNum: number
}


export const Pump: React.FC<PumpPropsType> = ({color, preasure, rpm, orderNum}) => {
    return (
        <div className={cn(scheme.scheme, style.pump, scheme[color])}>
            <h6>Pump{orderNum}</h6>
            <div className={style.inner}>
                <div>
                    <PumpIcon iconClass={scheme.pumpIcon}/>
                </div>
                <div className={style.date}>
                    <div className={style.preasure}>Preasure: <b>{preasure}</b></div>
                    <div className={style.rpm}>RPM: <b>{rpm}</b></div>
                </div>
            </div>
        </div>
    );
};