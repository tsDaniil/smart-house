import cn from "classnames";
import scheme from "../ContainerScheme.module.scss";
import style from './Tank.module.scss'
import {StatusesColors} from "../../../types/types";
import {PumpIcon} from "../Icons/PumpIcon";
import {TemperatureIncomingIcon} from "../Icons/TemperatureIncomingIcon";
import {TemperatureOutcomingIcon} from "../Icons/TemperatureOutcomingIcon";

type TankPropsType = {
    color: StatusesColors
    orderNum: number
    tempIncoming: number | null
    tempOutcoming: number | null
    level: string | null
}


export const Tank: React.FC<TankPropsType> = ({color, tempIncoming, tempOutcoming, orderNum, level}) => {
    return (
        <div className={style.tankOuter}>
            <div className={cn(scheme.scheme, style.tank, scheme[color])}>
                <div>
                    <TemperatureIncomingIcon iconClass={style.incomingTemp}/>
                    <div>T<sup>in</sup>: <b>+{tempIncoming}</b></div>
                </div>
                <div>
                    <h6>Tank{orderNum}</h6>
                    <div>Level: <b>{level}</b></div>
                </div>
                <div>
                    <TemperatureOutcomingIcon iconClass={style.outcomingTemp}/>
                    <div>T<sup>out</sup>: <b className={scheme.red}>+{tempOutcoming}</b></div>
                </div>
            </div>
        </div>
    );
};