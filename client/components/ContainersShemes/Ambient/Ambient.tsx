import cn from "classnames";
import mainStyle from "../../../styles/Main.module.scss";
import scheme from "../ContainerScheme.module.scss";
import style from './Ambient.module.scss'
import {TemperatureIncomingIcon} from "../Icons/TemperatureIncomingIcon";
import {HydroIcon} from "../Icons/HydroIcon";
import {StatusesColors} from "../../../types/types";

type AmbientPropsType = {
    color: StatusesColors
    temp: number
    hg: number
}


export const Ambient: React.FC<AmbientPropsType> = ({color, temp, hg}) => {
    return (
        <div className={cn(mainStyle.mb20, scheme.scheme, style.ambient, scheme[color])}>
            <div>
                <TemperatureIncomingIcon iconClass={scheme.incomingTemp}/>
                <div>T: <b>+{temp}</b></div>
            </div>
            <div>
                <h6>Ambient</h6>
            </div>
            <div>
                <HydroIcon iconClass={scheme.hydro}/>
                <div>Hg: <b>{hg}%</b></div>
            </div>
        </div>
    );
};