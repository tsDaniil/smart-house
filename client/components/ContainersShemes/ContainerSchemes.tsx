import style from './ContainerSchemes.module.scss'

import mainStyle from '../../styles/Main.module.scss'
import cn from "classnames";
import {Ambient} from "./Ambient/Ambient";
import {StatusesColors} from "../../types/types";
import {Pump} from "./Pump/Pump";
import {Tank} from "./Tank/Tank";
import {Cool} from "./Cool/Cool";

export const ContainerSchemes: React.FC<{  }> = props => {
    //Pump, Tank, Ambient, Cool
    // 'Warning': 'yellow'
    // 'Offline': 'gray'
    // 'Healthy': 'green'
    // 'Danger': 'red'
    return (
        <>
            <div className={style.schemes}>

                <Ambient color={StatusesColors.GREEN} temp={25} hg={25}/>

                <div className={cn(mainStyle.flexRow, mainStyle.mb8, style.table)}>
                    <div className={style.col}>
                        <div className={style.lines}/>

                        <Pump color={StatusesColors.GREEN} orderNum={0} preasure={20} rpm={600}/>

                        <Tank color={StatusesColors.RED} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                        <Tank color={StatusesColors.GRAY} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                        <Tank color={StatusesColors.YELLOW} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                        <Tank color={StatusesColors.GREEN} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                    </div>

                    <div className={style.col}>
                        <div className={style.lines}/>
                        <Pump color={StatusesColors.YELLOW} orderNum={0} preasure={23} rpm={600}/>

                        <Tank color={StatusesColors.RED} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                        <Tank color={StatusesColors.GRAY} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                        <Tank color={StatusesColors.YELLOW} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                        <Tank color={StatusesColors.GREEN} orderNum={0} tempIncoming={23} tempOutcoming={85} level={'LOW'}/>
                    </div>

                </div>

                <Cool color={StatusesColors.GREEN} tempIncoming={23} tempOutcoming={65} coolName={'ASI-COOL200'}/>
            </div>

        </>
    );
};