import {FC} from "react";

type UserLogoPropsType = {
    size: number
    src?: string
}

export const UserLogo: FC<UserLogoPropsType> = ({size, src}) => {
    return (
        <img
            src={src ? src : '/images/common-avatar.png'}
            alt="User avatar"
            width={size}
            height={size}
        />
    );
};