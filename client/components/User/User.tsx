import style from './User.module.scss';
import mainStyle from '../../styles/Main.module.scss';
import React from 'react';
import { UserLogo } from './UserLogo';
import { LogoutIcon } from './LogoutIcon';
import { useRouter } from 'next/router';
import cn from 'classnames';
import { logoutFx } from '../../models/auth';

type UserPropsType = {
    breakPoint?: string
}


export const User: React.FC<UserPropsType> = ({ breakPoint }) => {
    const router = useRouter();
    const onClickHandler = () => logoutFx()
        .then(() => router.replace('/authorization'));

    return (
        <div className={cn(style.userBlock, breakPoint)}>
            <div className={mainStyle.flexRowCenter}>
                <UserLogo size={32}/>
                <span className={style.userName}>John Karmilicin</span>
            </div>
            <a onClick={onClickHandler}>
                <LogoutIcon/>
            </a>
        </div>
    );
};
