import {LoginIcon} from "./Icons/LoginIcon";
import style from './AuthorizationForm.module.scss'
import {Button} from "../Button/Button";
import Link from "next/link";
import {loginForm, loginFx} from "../../models/auth";
import {useForm} from "effector-forms";
import {useStore} from "effector-react";
import {InputSize} from "../../types/types";
import {FormEvent} from "react";
import {PasswordTextField} from "../PasswordTextField/PasswordTextField";
import {TextFieldWithIcon} from "../TextFieldWithIcon/TextFieldWithIcon";

export const AuthorizationForm: React.FC<{}> = props => {

    const { fields, submit, eachValid } = useForm(loginForm)
    const pending = useStore(loginFx.pending)

    const onSubmit = (e:FormEvent) => {
        e.preventDefault()
        submit()
    }

    return (
        <form onSubmit={onSubmit} className={style.form}>
            <div className={style.field}>
                <TextFieldWithIcon
                    Icon={LoginIcon}
                    type='text'
                    name='login'
                    placeholder='Login'
                    inputSize={InputSize.LG}
                    value={fields.email.value}
                    disabled={pending}
                    onChange={(e) => fields.email.onChange(e.target.value)}
                />
                <div>
                    {fields.email.errorText({
                        "email": "you must enter a valid email address",
                    })}
                </div>
            </div>
            <div className={style.field}>
                <PasswordTextField
                    type='password'
                    name='password'
                    placeholder='Password'
                    inputSize={InputSize.LG}
                    value={fields.password.value}
                    disabled={pending}
                    onChange={(e) => fields.password.onChange(e.target.value)}
                />
                <div>
                    {fields.password.errorText({
                        "required": "password required"
                    })}
                </div>
            </div>
            <div className={style.submitBtn}>
                <Button type={'submit'}>
                    Login
                </Button>
            </div>
            <Link href={'/forgot_password'}>
                <a className={style.link}>Forgot Password </a>
            </Link>
        </form>
    );
};