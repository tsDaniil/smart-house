import style from './ContainersStatuses.module.scss';
import {ContainersStatus} from "./ContainersStatus";
import {containersMock} from "../../pages";




export const ContainersStatuses = () => {
    const container_statuses = [
        {statusName: 'Healthy', color: 'green' as const},
        {statusName: 'Warning', color: 'yellow' as const},
        {statusName: 'Danger', color: 'red' as const},
        {statusName: 'Offline', color: 'gray' as const},
    ].map(status => ({...status, containersNumber: containersMock.filter(container => container.status === status.statusName).length}))

    return (
        <div className={style.containersStatusesBlock}>
            <ContainersStatus containersNumber={containersMock.length}/>
            {container_statuses.map(status => <ContainersStatus key={status.statusName} status={status}/>)}
        </div>
    )
}
