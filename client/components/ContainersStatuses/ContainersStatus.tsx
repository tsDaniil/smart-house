import {FC} from "react";
import style from './ContainersStatus.module.scss';
import mainStyle from '../../styles/Main.module.scss';
import cn from "classnames";
import Link from "next/link";

type ContainersStatusPropsType = {
    status?: {
        statusName: string
        color: 'red' | 'yellow' | 'gray' | 'green'
        containersNumber: number
    }
    containersNumber?: number
}

export const ContainersStatus: FC<ContainersStatusPropsType> = ({status, containersNumber}) => {
    if (status) {
        return (
            <div className={style.containersStatus}>
                <div className={style.statusName}>
                    {status.statusName}
                </div>
                <div className={cn(style.separator, mainStyle[status?.color])}/>
                <div className={style.containersNumber}>
                    {status.containersNumber}
                </div>
            </div>
        );
    }
    if (containersNumber) {
        return (
            <div className={style.containersStatus}>
                <div className={style.link}>
                    <Link href={'/containers'}>
                        <a>Containers</a>
                    </Link>
                </div>
                <div className={style.containersNumber}>
                    {containersNumber}
                </div>
            </div>
        );
    }

    return null

};