import { components } from "react-select";
import {OptionProps} from "react-select/dist/declarations/src/components/Option";
import {CheckBox} from "../CheckBox/CheckBox";

export const OptionWithCheckbox = (props: OptionProps) => {
    const clickHandler = (e: React.MouseEvent) => {
        props.selectOption(props.data);
        e.stopPropagation();
        e.preventDefault();
    }
    return (
        <div>
            <components.Option {...props}>
                <div className="multi-select-option" onClick={clickHandler}>
                    <CheckBox text={props.label} checked={props.isSelected} onChange={()=>false}/>{" "}
                </div>
            </components.Option>
        </div>
    );
};