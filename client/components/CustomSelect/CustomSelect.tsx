import {FC} from "react";
import style from './CustomSelect.module.scss'
import Select, {GroupBase, StylesConfig} from 'react-select';
import {Props} from "react-select/dist/declarations/src/Select";
import {StateManagerAdditionalProps} from "react-select/dist/declarations/src/useStateManager";

export type OptionType = {
    value: string | number
    label: string | number
}
type CustomSelectPropsType = Props<OptionType, true, GroupBase<OptionType>> | StateManagerAdditionalProps<OptionType>

export const CustomSelect: FC<CustomSelectPropsType> = (props) => {
    const styles: StylesConfig<OptionType, true> = {
        option: (provided, state) => ({
            ...provided,
            fontWeight: state.isSelected ? 'bold' : 'normal',
            backgroundColor: state.isSelected ? '#ffffff' : '#ffffff',
        }),
        menuPortal: ((provided) => ({
            ...provided,
            zIndex: 110,
        })),

    }
    return (
       <Select
            {...props}
            styles={styles}
            className={style.customSelect}
            captureMenuScroll
            components={{
                IndicatorSeparator: () => null,
            }}
            />
    );
};