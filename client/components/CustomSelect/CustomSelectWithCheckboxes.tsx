import {ComponentType, FC} from "react";
import style from './CustomSelect.module.scss'
import Select, {GroupBase, OptionProps, StylesConfig} from 'react-select';
import {OptionWithCheckbox} from './OptionWithCheckbox'
import {Props} from "react-select/dist/declarations/src/Select";
import {OptionType} from "./CustomSelect";
import {StateManagerAdditionalProps} from "react-select/dist/declarations/src/useStateManager";

type CustomSelectWithCheckboxesPropsType = Props<OptionType, true,  GroupBase<OptionType>> | StateManagerAdditionalProps<OptionType>

export const CustomSelectWithCheckboxes: FC<CustomSelectWithCheckboxesPropsType> = (props) => {
    const styles: StylesConfig<OptionType, true> = {
        option: (provided, state) => ({
            ...provided,
            fontWeight: state.isSelected ? 'bold' : 'normal',
            backgroundColor: state.isSelected ? '#ffffff' : '#ffffff',
        }),
        menuPortal: ((provided) => ({
            ...provided,
            zIndex: 110,
        })),

    }
    return (
        <Select
            {...props}
            styles={styles}
            className={style.customSelect}
            captureMenuScroll
            isSearchable
            isMulti
            isClearable={false}
            controlShouldRenderValue={false}
            hideSelectedOptions={false}
            closeMenuOnSelect={false}
            blurInputOnSelect={false}
            menuShouldScrollIntoView={false}
            components={{
                IndicatorSeparator: () => null,
                Option : OptionWithCheckbox as  ComponentType<OptionProps<OptionType, boolean, GroupBase<OptionType>>> | undefined
            }}
        />
    );
};