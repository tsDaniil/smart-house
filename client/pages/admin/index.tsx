import React, {useContext} from 'react'
import type {NextPage} from 'next'
import Head from 'next/head'
import {SideBar} from '../../components/SideBar/SideBar'
import {User} from '../../components/User/User'
import main from '../../styles/Main.module.scss'
import {Button} from "../../components/Button/Button";
import {TableSearch} from "../../components/Tables/TableSearch";
import {InfoBlocks} from "../../components/InfoBlocks/InfoBlocks";
import {PlusIcon} from "../../components/Button/Icons/PlusIcon";
import {CheckBox} from "../../components/CheckBox/CheckBox";
import {ButtonColorThemes, ButtonVariant} from "../../types/types";
import {GlobalModalContext, MODAL_TYPES} from "../../components/Modals/GlobalModal";
import {AdminUsersTable} from "../../components/Tables/AdminUsersTable/AdminUsersTable";
import cn from "classnames";
import {LogoIcon} from "../../components/SideBar/Icons/LogoIcon";
import {MobileMenu} from "../../components/MobileMenu/MobileMenu";


const Home: NextPage = () => {
    const { showModal } = useContext(GlobalModalContext);
    const createAddUserModal = () => {
        showModal(
            MODAL_TYPES.ADD_USER_MODAL,
            {
                modalTitle: 'Add new user',
            }
        )
    };

    return (
        <>
            <Head>
                <title>Dashboard</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <div className={'background'}>
                <div className={'container'}>
                    <div className={cn(main.mobileNav)}>
                        <LogoIcon iconClass={main.mobileLogo}/>
                        <MobileMenu/>
                    </div>
                    <SideBar/>
                    <div className={main.rightPanel}>
                        <header>
                            <h1>Users</h1>
                            <User breakPoint={main.lgFlex}/>
                        </header>
                        <div className={main.subHeader}>
                            <InfoBlocks/>
                            <div className={main.addUserBtn}>
                                <Button textWeight={700} fontSize={18} onClick={()=>createAddUserModal()}>
                                    <PlusIcon/>
                                    &nbsp;
                                    Add user
                                </Button>
                            </div>
                        </div>
                        <main className={main.mb28}>
                            <div className={main.tableBlock}>
                                <div className={main.tableFilters}>
                                    <div className={main.flexRowCenterCenter}>
                                        <CheckBox text={'Client'} name='client'/>
                                        <CheckBox text={'Lead'} name='lead'/>
                                    </div>
                                    <div className={main.tableSearch}>
                                        <TableSearch/>
                                    </div>
                                </div>

                                <AdminUsersTable/>
                            </div>
                            <div className={main.loadMoreBtn}>
                                <Button variant={ButtonVariant.UNDERLINED} colorTheme={ButtonColorThemes.white}>Load more</Button>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Home