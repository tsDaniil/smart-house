import React, {useContext} from 'react'
import type {NextPage} from 'next'
import Head from 'next/head'
import {SideBar} from '../../components/SideBar/SideBar'
import {User} from '../../components/User/User'
import main from '../../styles/Main.module.scss'
import {Button} from "../../components/Button/Button";
import {TableSearch} from "../../components/Tables/TableSearch";
import {InfoBlocks} from "../../components/InfoBlocks/InfoBlocks";
import {PlusIcon} from "../../components/Button/Icons/PlusIcon";
import cn from "classnames";
import {ButtonColorThemes, ButtonVariant} from "../../types/types";
import {containersMock} from "../index";
import {GlobalModalContext, MODAL_TYPES} from "../../components/Modals/GlobalModal";
import {AdminContainersTable} from "../../components/Tables/AdminContainersTable/AdminContainersTable";
import {LogoIcon} from "../../components/SideBar/Icons/LogoIcon";
import {MobileMenu} from "../../components/MobileMenu/MobileMenu";
import {ADMIN_CONTAINERS_TABLE_HEADERS} from "../../constants/CONSTANTS";

const Containers: NextPage = () => {
    const {showModal} = useContext(GlobalModalContext);
    const createDeleteModal = () => {
        showModal(
            MODAL_TYPES.ADD_CONTAINER_MODAL,
            {
                modalTitle: 'Add container',
            }
        )
    };
    return (
        <>
            <Head>
                <title>Containers</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <div className={'background'}>
                <div className={'container'}>
                    <div className={cn(main.mobileNav)}>
                        <LogoIcon iconClass={main.mobileLogo}/>
                        <MobileMenu/>
                    </div>
                    <SideBar/>
                    <div className={main.rightPanel}>
                        <header>
                            <h1>Containers</h1>
                            <User breakPoint={main.lgFlex}/>
                        </header>
                        <div className={main.subHeader}>
                            <InfoBlocks/>
                            <div className={main.addContainerBtn}>
                                <Button textWeight={700} fontSize={18} onClick={createDeleteModal}>
                                    <PlusIcon/>
                                    &nbsp;
                                    Add Container
                                </Button>
                            </div>
                        </div>
                        <main className={main.mb28}>
                            <div className={main.tableBlock}>
                                <div className={cn(main.tableSearch, main.mlAuto)}>
                                    <TableSearch/>
                                </div>

                                <AdminContainersTable containers={containersMock}
                                                      tableHeaders={ADMIN_CONTAINERS_TABLE_HEADERS}/>
                            </div>
                            <div className={main.loadMoreBtn}>
                                <Button variant={ButtonVariant.UNDERLINED} colorTheme={ButtonColorThemes.white}>Load
                                    more</Button>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Containers