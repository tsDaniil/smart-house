import React from 'react'
import type {NextPage} from 'next'
import Head from 'next/head'
import {SideBar} from '../../components/SideBar/SideBar'
import {User} from '../../components/User/User'
import main from '../../styles/Main.module.scss'
import Link from "next/link";
import {ContainerSchemes} from "../../components/ContainersShemes/ContainerSchemes";
import cn from "classnames";
import {LogoIcon} from "../../components/SideBar/Icons/LogoIcon";
import {MobileMenu} from "../../components/MobileMenu/MobileMenu";

const containerMock = {serial: 586123, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'}

const Container: NextPage = () => {
    return (
        <>
            <Head>
                <title>Container</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <div className={'background'}>
                <div className={'container'}>
                    <div className={cn(main.mobileNav)}>
                        <LogoIcon iconClass={main.mobileLogo}/>
                        <MobileMenu/>
                    </div>
                    <SideBar/>
                    <div className={main.rightPanel}>
                        <header>
                            <h1>
                                <Link href={'/containers'}>
                                    <a className={main.breadCrumbLink}>Containers</a>
                                </Link>
                                <span> / {containerMock.model}</span>
                            </h1>
                            <User breakPoint={main.lgFlex}/>
                        </header>
                        <main>
                            <div className={main.tableBlock}>
                                <ContainerSchemes/>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Container