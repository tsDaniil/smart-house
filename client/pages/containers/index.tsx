import React from 'react'
import type {NextPage} from 'next'
import Head from 'next/head'
import {SideBar} from '../../components/SideBar/SideBar'
import {User} from '../../components/User/User'
import main from '../../styles/Main.module.scss'
import {containersMock} from "../index";
import {Button} from "../../components/Button/Button";
import {TableSearch} from "../../components/Tables/TableSearch";
import cn from "classnames";
import {ButtonColorThemes, ButtonVariant} from "../../types/types";
import {UserContainersTable} from "../../components/Tables/UserContainersTable/UserContainersTable";
import {LogoIcon} from "../../components/SideBar/Icons/LogoIcon";
import {MobileMenu} from "../../components/MobileMenu/MobileMenu";
import {CONTAINERS_TABLE_HEADERS} from "../../constants/CONSTANTS";

const Containers: NextPage = () => (
    <>
        <Head>
            <title>Containers</title>
            <meta charSet="utf-8"/>
            <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        </Head>
        <div className={'background'}>
            <div className={'container'}>
                <div className={cn(main.mobileNav)}>
                    <LogoIcon iconClass={main.mobileLogo}/>
                    <MobileMenu/>
                </div>
                <SideBar/>
                <div className={main.rightPanel}>
                    <header>
                        <h1>Containers</h1>
                        <User breakPoint={main.lgFlex}/>
                    </header>
                    <main className={main.mb28}>
                        <div className={main.tableBlock}>
                            <div className={cn(main.tableSearch, main.mlAuto)}>
                                <TableSearch/>
                            </div>
                            <UserContainersTable tableHeaders={CONTAINERS_TABLE_HEADERS} containers={containersMock}/>
                        </div>
                        <div className={main.loadMoreBtn}>
                            <Button variant={ButtonVariant.UNDERLINED} colorTheme={ButtonColorThemes.white}>Load more</Button>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </>
)

export default Containers