import '../styles/globals.scss'
import type { AppProps } from 'next/app'
import '../models/init';
import {GlobalModal} from "../components/Modals/GlobalModal";

function MyApp({ Component, pageProps }: AppProps) {
  return <GlobalModal>
    <Component {...pageProps} />
  </GlobalModal>
}

export default MyApp
