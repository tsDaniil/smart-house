import React, { useEffect } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import main from '../../styles/Main.module.scss';
import { AuthorizationForm } from '../../components/AuthorizationForm/AuthorizationForm';
import { LogoIcon } from '../../components/SideBar/Icons/LogoIcon';
import { useStore } from 'effector-react';
import { $userEmail, checkUserFx } from '../../models/auth';
import { useRouter } from 'next/router';

const Index: NextPage = () => {
    const email = useStore($userEmail);
    const router = useRouter();

    useEffect(() => {
        if (!email) {
            checkUserFx();
        } else {
            router.replace('/');
        }
    }, []);

    useEffect(() => {
        if (email) {
            router.replace('/');
        }
    }, [email]);

    return (
        <>
            <Head>
                <title>Authorization</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <div className={main.loginPage}>
                <LogoIcon iconClass={main.logo}/>
                <AuthorizationForm/>
            </div>

        </>
    );
};

export default Index;