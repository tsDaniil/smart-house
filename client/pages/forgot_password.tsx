import React from 'react'
import type {NextPage} from 'next'
import Head from 'next/head'
import main from '../styles/Main.module.scss'
import {LogoIcon} from "../components/SideBar/Icons/LogoIcon";
import {ForgotPasswordForm} from "../components/ForgotPasswordForm/ForgotPasswordForm";

const ForgotPassword: NextPage = () => (
    <>
        <Head>
            <title>Reset Password</title>
            <meta charSet="utf-8"/>
            <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        </Head>
        <div className={main.loginPage}>
            <LogoIcon iconClass={main.logo}/>
            <ForgotPasswordForm/>
        </div>
    </>
)

export default ForgotPassword