import type {NextPage} from 'next'
import Head from 'next/head'
import mainStyle from '../styles/Main.module.scss'
import {SideBar} from '../components/SideBar/SideBar';
import React, {useMemo} from 'react'
import {User} from '../components/User/User'
import {ContainersStatuses} from "../components/ContainersStatuses/ContainersStatuses";
import {TableTitle} from "../components/Tables/TableTitle";
import {ContainerType, StatusesColors} from "../types/types";
import {UserContainersTable} from "../components/Tables/UserContainersTable/UserContainersTable";
import cn from "classnames";
import main from "../styles/Main.module.scss";
import {LogoIcon} from "../components/SideBar/Icons/LogoIcon";
import {MobileMenu} from "../components/MobileMenu/MobileMenu";
import {CONTAINERS_TABLE_HEADERS} from "../constants/CONSTANTS";


export const containersMock = [
    {id:1, serial: 586123, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:2, serial: 586124, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:3, serial: 586125, name: '', model: 'ASI-C20HQ', status: 'Danger'},
    {id:4, serial: 586126, name: '3 row 2 level', model: 'ASI-COOL200', status: 'Danger'},
    {id:5, serial: 586127, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:6, serial: 586128, name: '', model: 'ASI-C20HQ', status: 'Danger'},
    {id:7, serial: 586129, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:8, serial: 586130, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:9, serial: 586131, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:10, serial: 586138, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:11, serial: 586139, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:12, serial: 586140, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Danger'},
    {id:13, serial: 586132, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Warning'},
    {id:14, serial: 586133, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Warning'},
    {id:15, serial: 586134, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Warning'},
    {id:16, serial: 586135, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Offline'},
    {id:17, serial: 586136, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Offline'},
    {id:18, serial: 586137, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Offline'},
    {id:19, serial: 586138, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Healthy'},
    {id:20, serial: 586139, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Healthy'},
    {id:21, serial: 586140, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Healthy'},
    {id:22, serial: 586141, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Healthy'},
    {id:23, serial: 586142, name: 'Greenbox with blue dots', model: 'ASI-C20HQ', status: 'Healthy'},
]

const Home: NextPage = () => {

    const containers = {danger:[] as ContainerType[], warning: [] as ContainerType[], offline: [] as ContainerType[]};
    useMemo(() =>containersMock.forEach(container => {
        if(container.status === 'Danger') containers.danger.push(container)
        if(container.status === 'Warning') containers.warning.push(container)
        if(container.status === 'Offline') containers.offline.push(container)
    }), [])
    return (
        <>
            <Head>
                <title>Dashboard</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <div className={'background'}>
                <div className={'container'}>
                    <div className={cn(main.mobileNav)}>
                        <LogoIcon iconClass={main.mobileLogo}/>
                        <MobileMenu/>
                    </div>
                    <SideBar/>
                    <div className={mainStyle.rightPanel}>
                        <header>
                            <h1>Dashboard</h1>
                            <User breakPoint={mainStyle.lgFlex}/>
                        </header>
                        <main>
                            <ContainersStatuses/>

                            <div className={mainStyle.tableBlock}>
                                <TableTitle dotColor={StatusesColors.RED}>
                                    Danger
                                </TableTitle>
                                <UserContainersTable
                                    containers={containers.danger}
                                    tableHeaders={CONTAINERS_TABLE_HEADERS}
                                />
                            </div>

                            <div className={mainStyle.tableBlock}>
                                <TableTitle dotColor={StatusesColors.YELLOW}>
                                    Warning
                                </TableTitle>
                                <UserContainersTable
                                    containers={containers.warning}
                                    tableHeaders={CONTAINERS_TABLE_HEADERS}
                                />
                            </div>

                            <div className={mainStyle.tableBlock}>
                                <TableTitle dotColor={StatusesColors.GRAY}>
                                    Offline
                                </TableTitle>
                                <UserContainersTable
                                    containers={containers.offline}
                                    tableHeaders={CONTAINERS_TABLE_HEADERS}
                                />
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </>
    )
}


export default Home
