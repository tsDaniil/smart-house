import { createUsersTable } from './db/initialize/createTables/createUsersTable';
import { addAdminUser } from './db/initialize/addAdminUser';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { createServer } from './server';

dayjs.extend(utc);

createUsersTable()
    .then(addAdminUser)
    .then(createServer);
