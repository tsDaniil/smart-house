import { BadRequest } from '../errors';
import { ValidationError } from 'yup';
import { AnySchema } from 'yup/lib/schema';
import { Middleware } from 'koa';
import { info } from '../utils/log';

export const validateByYupM = <Schema extends AnySchema<any, any, any>, From extends 'body' | 'query'>(schema: Schema, from: From): ValidateYupMiddleware<Schema, any, From> => (ctx, next) => {
    return schema.validate(ctx.request[from], { abortEarly: false })
        .catch((details: ValidationError) => {
            const errorHash = details.inner.reduce(
                (acc, item) =>
                    Object.assign(acc, { [item.path ?? item.name]: item.message }),
                Object.create(null)
            );

            return Promise.reject(new BadRequest(Object.entries(errorHash).reduce((message, [field, error]) => {
                return `${(message ? message + '\n' : message)}Field "${field}" is invalid. Details: "${error}".`
            }, '')));
        })
        .then(() => next());
}

type ExtractYupOut<T extends AnySchema<any, any, any>> = T extends AnySchema<any, any, infer R> ? R : never;

export type ValidateYupMiddleware<T extends AnySchema<any, any, any>, State = {}, From extends 'body' | 'query' = 'body'> =
    Middleware<State, { [Key in From]: ExtractYupOut<T> }>;
