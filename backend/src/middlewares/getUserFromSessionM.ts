import { Middleware } from 'koa';
import { UserState } from './checkUserM';
import { omit } from 'ramda';

export const getUserFromSessionM: Middleware<UserState> = (ctx, next) => {
    ctx.body = omit(['password'], ctx.state.user);
    return next();
};