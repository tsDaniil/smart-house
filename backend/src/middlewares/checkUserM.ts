import { Middleware } from 'koa';
import { BackendUser } from '../types/types';
import { CookieState } from './cookie';
import { parse } from '../services/token/token';
import { info } from '../utils/log';
import { AuthError } from '../errors';

export type UserState = CookieState<'Authorization'> & {
    user: BackendUser;
}

export const checkUserM: Middleware<UserState> = (ctx, next) => {
    const token = ctx.state.cookie?.Authorization ?? '';

    if (!token) {
        throw new AuthError('Has no token!');
    }

    info(`Check user Middleware. Token is "${token}".`);

    return parse(token)
        .then((user) => {
            ctx.state.user = user;
            return next();
        });
};