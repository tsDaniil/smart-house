import Router from 'koa-router';
import { getCookieM } from '../middlewares/cookie';
import { checkUserM, UserState } from '../middlewares/checkUserM';
import { getUserFromSessionM } from '../middlewares/getUserFromSessionM';
import { validateByYupM } from '../middlewares/validateByYupM';
import { loginParamsSchema } from '../api/user/loginParamsSchema';
import { loginM } from '../api/user/loginM';
import { logoutM } from '../api/user/logoutM';

export const router = new Router();
export const privateRouter = new Router<UserState>();

router
    .post('/login', validateByYupM(loginParamsSchema, 'body'), loginM)
    .post('/sign-up');

privateRouter
    .use(getCookieM(['Authorization']))
    .use(checkUserM)
    .get('/user', getUserFromSessionM)
    .post('/logout', logoutM);

