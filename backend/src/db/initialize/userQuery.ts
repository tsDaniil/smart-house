import { BackendUser } from '../../types/types';
import { knex } from '../connect';
import { userFromBd } from '../../utils/schemas';

export const getUserByUniqProp = <Prop extends 'id' | 'email'>(prop: Prop, value: BackendUser[Prop]): Promise<BackendUser | null> =>
    knex
        .from('users')
        .select('*')
        .where(prop, '=', value)
        .first()
        .then<BackendUser | null>(user => user ? userFromBd(user) : null);