import { knex } from '../connect';
import { ADMIN_USER_EMAIL, ADMIN_USER_NAME, ADMIN_USER_PASSWORD } from '../../constatns';
import { __, always, gte, pipe, prop } from 'ramda';
import { info } from '../../utils/log';

const hasAdmin = (): Promise<boolean> =>
    knex
        .from('users')
        .where('role', '=', 'admin')
        .count()
        .first<any, { cont: number }>()
        .then(pipe<[{ count: number }], number, boolean>(prop('count'), gte(__, 1)));


export const addAdminUser = () =>
    hasAdmin()
        .then((hasAdmin) => {
            if (hasAdmin) {
                info('Admin user is exits!');
                return void 0;
            }
            info('Create admin user!');
            return knex
                .into('users')
                .insert({
                    email: ADMIN_USER_EMAIL,
                    name: ADMIN_USER_NAME,
                    password: ADMIN_USER_PASSWORD, // TODO add hex
                    role: 'admin'
                })
                .onConflict().ignore()
                .then(always(void 0));
        });