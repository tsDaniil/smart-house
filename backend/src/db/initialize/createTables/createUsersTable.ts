import { checkTable } from '../../utils/checkTable';
import { knex } from '../../connect';

export const createUsersTable = () =>
    checkTable('users', (tableBuilder) => {
        tableBuilder.increments('id').primary().comment('Auto-generated id');
        tableBuilder.string('password').notNullable().comment('Hex of user password');
        tableBuilder.string('email').notNullable().unique();
        tableBuilder.string('role').notNullable();
        tableBuilder.string('name').notNullable();
        tableBuilder.timestamp('createdAt', { useTz: true }).defaultTo(knex.fn.now());
    });