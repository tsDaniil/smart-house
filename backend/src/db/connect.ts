import createKnex from 'knex';
import { DATA_BASE_HOST, DATA_BASE_PASSWORD, DATA_BASE_PORT } from '../constatns';

export const knex = createKnex({
    client: 'pg',
    connection: {
        user: 'postgres',
        host: DATA_BASE_HOST,
        port: DATA_BASE_PORT,
        database: 'postgres',
        password: DATA_BASE_PASSWORD
    }
});