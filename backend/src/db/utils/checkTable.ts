import { knex } from '../../db/connect';
import { always, equals, ifElse, pipe } from 'ramda';
import { Knex } from 'knex';
import CreateTableBuilder = Knex.CreateTableBuilder;
import { error, info } from '../../utils/log';

const logOnCall = (message: string) => () => {
    info('Check table', message);
};

export const checkTable = <DB_NAME extends Knex.TableNames>(db_name: DB_NAME, make_table_cb: Callback) =>
    knex
        .schema
        .hasTable(db_name)
        .then(ifElse<[boolean], void, Promise<void>>(
            equals(true as boolean),
            logOnCall(`Table "${db_name}" is exists!`),
            pipe(
                logOnCall(`Create "${db_name}" table! Has table is`),
                () => knex.schema.createTable(db_name, make_table_cb)
                    .then(always(void 0))
            )
        ))
        .catch((e: Error) => {
            error('Check table', `Can't create table "${db_name}", ${e}`);
            return Promise.reject(e);
        });

type Callback = (tableBuilder: CreateTableBuilder) => void;