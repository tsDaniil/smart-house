export type UserRole = 'admin' | 'user';

export type BackendUser<DATE = Date> = {
    id: number;
    password: string; // md5
    email: string; // uniq
    name: string;
    role: UserRole;
    createdAt: DATE;
}

export type User<DATE = Date> = Omit<BackendUser<DATE>, 'password'>

export type Container = {
    id: number;
    type: 'A' | 'B';
};

export type Sensor = {
    id: number;

}

// USER_CONTAINER, где лежит USER.id and CONTAINER.id many to many
// CONTAINER_SENSOR, где лежит CONTAINER.id and SENSOR.id
