import { BackendUser } from '../../types/types';
import { Knex } from 'knex';


declare module 'knex/types/tables' {

    interface Tables {
        users: Knex.CompositeTableType<BackendUser<string>,
            Omit<BackendUser<string>, 'id' | 'createdAt'>,
            Partial<Omit<BackendUser, 'id'>>>;
    }
}

declare module 'knex/types/result' {
    interface Registry {
        Count: number;
    }
}