import { ValidateYupMiddleware } from '../../middlewares/validateByYupM';
import { LoginParamsSchema } from './loginParamsSchema';
import { getUserByUniqProp } from '../../db/initialize/userQuery';
import { BadRequest } from '../../errors';
import { info } from '../../utils/log';
import { create } from '../../services/token/token';
import { serialize } from 'cookie';
import { omit } from 'ramda';

export const loginM: ValidateYupMiddleware<LoginParamsSchema, {}, 'body'> = (ctx, next) => {
    const { email, password } = ctx.request.body;

    return getUserByUniqProp('email', email)
        .then((user) => {
            if (!user) {
                info(`Has no user with email ${email}`);
                throw new BadRequest('Incorrect email or password!');
            }
            if (user.password !== password) {
                info(`Wrong password!`);
                throw new BadRequest('Incorrect email or password!');
            }
            const token = create(user);
            ctx.res.setHeader('Set-Cookie', serialize('Authorization', token, {
                httpOnly: true,
                secure: true
            }));
            ctx.body = omit(['password'], user);
            info('Success login user!');

            return next();
        });
};