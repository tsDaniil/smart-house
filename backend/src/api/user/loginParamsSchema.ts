import { object, string } from 'yup';

export const loginParamsSchema = object().shape({
    email: string().required('Email is required field!').email('Invalid email!'),
    password: string().required('Password is required field!')
});

export type LoginParamsSchema = typeof loginParamsSchema;
