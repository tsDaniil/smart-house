import { ValidateYupMiddleware } from '../../middlewares/validateByYupM';
import { LoginParamsSchema } from './loginParamsSchema';
import { serialize } from 'cookie';

export const logoutM: ValidateYupMiddleware<LoginParamsSchema, {}, 'body'> = (ctx, next) => {
    ctx.res.setHeader('Set-Cookie', serialize('Authorization', '', {
        httpOnly: true,
        secure: true
    }));
    ctx.body = { ok: true };

    return next();
};