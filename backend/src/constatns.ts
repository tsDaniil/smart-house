import { __, identity, ifElse, includes, isNil, pipe, prop, tap } from 'ramda';
import { config } from 'dotenv';
import { error, info } from './utils/log';
import { join } from 'path';

config({
    path: join(__dirname, '..', '.env')
});

const AVAILABLE_PERIOD_VALUES = [
    'second',
    'minute',
    'hour',
    'day',
    'month'
] as const;

const makeException = (message: string) => (): never => {
    error(message);
    process.exit(1);
};

const getRequiredProp = pipe<[string], string, string | undefined, string>(
    tap(propName => info(`Get ${propName} from .env`)),
    prop(__, process.env),
    ifElse(isNil, makeException('Required prop is not provided in .env'), identity)
);

const getRequiredNumberProp = pipe(
    getRequiredProp,
    Number
);

type ArrayValues<T extends Readonly<Array<any>>> = T extends Readonly<Array<infer R>> ? R : never;

const getRequiredOneOf = <T extends Readonly<Array<string>>>(methodName: string, values: T) => pipe(
    getRequiredProp,
    ifElse(
        includes(__, AVAILABLE_PERIOD_VALUES) as (val: string) => val is ArrayValues<T>,
        identity,
        makeException(`Value is not one of "${values.join(', ')}"`)
    )
)(methodName);

export const REST_PORT = getRequiredNumberProp('REST_PORT');
export const SECRET = getRequiredProp('SECRET');
export const TOKEN_LIVE_VALUE = getRequiredNumberProp('TOKEN_LIVE_VALUE');
export const TOKEN_LIVE_PERIOD = getRequiredOneOf('TOKEN_LIVE_PERIOD', AVAILABLE_PERIOD_VALUES);
export const DATA_BASE_HOST = getRequiredProp('DATA_BASE_HOST');
export const DATA_BASE_PORT = getRequiredNumberProp('DATA_BASE_PORT');
export const DATA_BASE_PASSWORD = getRequiredProp('DATA_BASE_PASSWORD');
export const ADMIN_USER_EMAIL = getRequiredProp('ADMIN_USER_EMAIL');
export const ADMIN_USER_NAME = getRequiredProp('ADMIN_USER_NAME');
export const ADMIN_USER_PASSWORD = getRequiredProp('ADMIN_USER_PASSWORD');
