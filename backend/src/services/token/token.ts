import { BackendUser } from '../../types/types';
import { Token } from '../../protobuf/protos';
import { AuthError } from '../../errors';
import { BigNumber } from '@waves/bignumber';
import { base64Decode, base64Encode, publicKey, signBytes, stringToBytes, verifySignature } from '@waves/ts-lib-crypto';
import { SECRET, TOKEN_LIVE_PERIOD, TOKEN_LIVE_VALUE } from '../../constatns';
import { getUserByUniqProp } from '../../db/initialize/userQuery';
import dayjs from 'dayjs';
import { info } from '../../utils/log';

const getTokenBytes = (expireAt: number, scope: string, userId: number) => {
    const expireBytes = new BigNumber(expireAt).toBytes({ isSigned: false, isLong: true });
    const userIdBytes = new BigNumber(userId).toBytes({ isSigned: false, isLong: true });
    const scopeBytes = stringToBytes(scope, 'utf8');

    return Uint8Array.from([
        ...Array.from(expireBytes),
        ...Array.from(userIdBytes),
        ...Array.from(scopeBytes),
    ]);
};

export const parse = (tokenBase64: string): Promise<BackendUser> => {
    try {
        const token = Token.decode(base64Decode(tokenBase64));
        const bytes = getTokenBytes(Number(token.expiredAt.toString()), token.scope, token.userId);
        const isExpired = Date.now() > Number(token.expiredAt.toString());

        if (isExpired) {
            info('Parse token. Token expired!');
            return Promise.reject(new AuthError('Token expired!'));
        }

        const isValidSignature = verifySignature(publicKey(SECRET), bytes, token.signature);

        if (!isValidSignature) {
            info('Parse token. Invalid token signature!');
            return Promise.reject(new AuthError('Invalid token signature!'));
        }

        return getUserByUniqProp('id', token.userId)
            .then(user => {
                if (!user) {
                    info('Parse token. Has no user with id from token!');
                    return Promise.reject(new AuthError('Wrong token!'));
                }
                return user;
            });
    } catch (e) {
        info('Parse token. Invalid token.');
        return Promise.reject(new AuthError('Invalid token!'));
    }
};

export const create = (user: Pick<BackendUser, 'id' | 'role'>): string => {
    const accessTokenExpiredAt = dayjs().add(TOKEN_LIVE_VALUE, TOKEN_LIVE_PERIOD)
        .toDate()
        .getTime();

    const bytes = getTokenBytes(accessTokenExpiredAt, user.role, user.id);
    const signature = signBytes(SECRET, bytes);
    const token = Token.encode({
        expiredAt: accessTokenExpiredAt,
        scope: user.role,
        signature,
        type: Token.TYPE.ACCESS,
        userId: user.id
    }).finish();

    return base64Encode(token);
};
