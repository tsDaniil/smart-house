import { construct, evolve, pipe } from 'ramda';
import { BackendUser } from '../types/types';


export const userFromBd: (user: BackendUser<string>) => BackendUser = evolve({
    createdAt: construct(Date)
});

export const userToBd: (user: BackendUser) => BackendUser<string> = evolve({
    createdAt: (date: Date) => date.toISOString()
});
