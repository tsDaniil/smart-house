import dayjs from 'dayjs';

const getPrefix = () =>
    `${dayjs().format(`DD.MM.YYYY HH:mm:ss`)}:`;

const stringify = (data: any): string => {
    switch (typeof data) {
        case 'boolean':
        case 'number':
        case 'function':
        case 'bigint':
        case 'undefined':
        case 'symbol':
            return String(data);
        case 'object':
            return JSON.stringify(data, null, 4);
        case 'string':
            return data;
    }
}

const COLORS = {
    blue: '\x1b[34m',
    yellow: '\x1b[33m',
    red: '\x1b[31m'
};
const RESET = '\x1b[0m';
const SEPARATOR = '%s'

export const log = (...args: Array<any>) =>
    console.log(getPrefix(), ...args.map(stringify));

export const info = (...args: Array<any>) =>
    console.log(getPrefix(), COLORS.blue, ...args.map(stringify), RESET);

export const error = (...args: Array<any>) =>
    console.log(getPrefix(), COLORS.red, ...args.map(stringify), RESET);

export const warn = (...args: Array<any>) =>
    console.log(getPrefix(), COLORS.yellow, ...args.map(stringify), RESET);
