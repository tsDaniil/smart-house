import Koa from 'koa';
import { setHeadersM } from './middlewares/headers';
import { applyErrorM } from './errors';
import { privateRouter, router } from './router/router';
import koaBody from 'koa-body';
import mount from 'koa-mount';
import createStaticServer from 'koa-static';
import { join } from 'path';
import { REST_PORT } from './constatns';
import { info } from './utils/log';

export const createServer = () => {
    const apiServer = new Koa();
    const staticServer = new Koa();
    const app = new Koa();

    staticServer
        .use(setHeadersM({
            'Access-Control-Allow-Origin': 'http://localhost:3000',
            'Access-Control-Allow-Headers': 'Content-Type'
        }))
        .use(createStaticServer(join(__dirname, '..', '..', 'client', 'out'), {
            maxage: 60 * 5,
        }));

    apiServer
        .use(koaBody({ json: true }))
        .use(setHeadersM({
            'Access-Control-Allow-Origin': 'http://localhost:3000',
            'Access-Control-Allow-Credentials': 'true',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type,Authorization'
        }))
        .use(applyErrorM)
        .use(router.routes())
        .use(router.allowedMethods())
        .use(privateRouter.routes())
        .use(privateRouter.allowedMethods());

    app
        .use(mount('/api', apiServer))
        .use(mount('/', staticServer));

    app
        .listen(REST_PORT)

    info(`Launch server on port ${REST_PORT}`);
};
