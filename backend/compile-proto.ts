import * as pbjs from 'protobufjs/cli/pbjs';
import * as pbts from 'protobufjs/cli/pbts';
import { promises } from 'fs';
import { join, parse } from 'path';
import yargs from 'yargs/yargs';
import { asyncMap } from '@tsigel/async-map';
import { error, info } from 'npmlog';


const { path, out } = yargs(process.argv.slice(2)).options({
    out: {
        required: true,
        alias: 'o',
        type: 'string'
    },
    path: {
        required: true,
        alias: 'p',
        type: 'string'
    }
}).parseSync();

const compileProtoFile = (filePath: string) => {
    info('Proto compile', `Compile proto file with path ${filePath}`);
    const { dir, name } = parse(filePath);
    return new Promise((res, rej) => {
            pbjs.main([
                '--no-create',
                '--no-verify',
                '--no-convert',
                // '--keep-case',
                '-p', dir,
                '-r', name,
                '-t', 'static-module',
                '-w', 'commonjs',
                '-o', join(__dirname, out, `${name}.js`),
                join(dir, `${name}.proto`)
            ], (err: Error | null, data: any) => {
                if (err) {
                    error('Proto compile', name, 'error\n', err);
                    rej(err);
                } else {
                    info('Proto compile', name, 'done');
                    res(data);
                }
            });
        }
    ).then(() => new Promise((res, rej) => {
        pbts.main([
            '-o',
            join(out, `${name}.d.ts`),
            join(out, `${name}.js`)
        ], (err: Error | null, data: any) => {
            if (err) {
                error('Proto dts compile', name, 'error\n', err);
                rej(err);
            } else {
                info('Proto dts compile', name, 'done');
                res(data);
            }
        });
    }));
};

const compileDir = (path: string): Promise<unknown> =>
    promises.mkdir(join(__dirname, out))
        .catch(() => null)
        .then(() => promises.readdir(path))
        .then(files =>
            asyncMap(3, (name: string) =>
                promises.stat(join(path, name))
                    .then((stat) => ({ name, isDir: stat.isDirectory() })), files))
        .then((list) =>
            asyncMap<{ name: string, isDir: boolean }, unknown>(3, ({ name, isDir }: any) =>
                    isDir
                        ? compileDir(join(path, name))
                        : compileProtoFile(join(path, name)),
                list)
        );

compileDir(join(__dirname, path))
    .then(() => info('Proto compile', `All is done!`));
